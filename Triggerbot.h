#pragma once

#include "Includes.h"

struct EntityData
{
	DWORD LocalPlayer; //LocalPlayer variable
	DWORD enemyInChrosshair;
	int id;
	int myTeam;
	int entityTeam;
	int entityHealth;

	void readMemory()
	{
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwLocalPlayer), &LocalPlayer, sizeof(DWORD), 0); //Reading LocalPlayer
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(LocalPlayer + hazedumper::netvars::m_iCrosshairId), &id, sizeof(int), 0); //Reading Enemy ID
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(LocalPlayer + hazedumper::netvars::m_iTeamNum), &myTeam, sizeof(int), 0); //Reading myTeam
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwEntityList + ((id - 1) * 0x10)), &enemyInChrosshair, sizeof(DWORD), 0); //Reading EnemyInCrosshair
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(enemyInChrosshair + hazedumper::netvars::m_iTeamNum), &entityTeam, sizeof(int), 0); //Reading EnemyTeam
		//ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(enemyInChrosshair + hazedumper::netvars::m_iHealth), &entityHealth, sizeof(int), 0); //Reading Enemy's Health
	}

}EntityDataTrigger;

void runTrigger()
{
	int doattack;
	int IsShooting;
	while (true)
	{
		EntityDataTrigger.readMemory();

		/*if (GetAsyncKeyState(VK_RBUTTON))
		{*/
			if (EntityDataTrigger.myTeam != EntityDataTrigger.entityTeam /*&& EntityDataTrigger.entityHealth > 0*/ && EntityDataTrigger.id > 0 && EntityDataTrigger.id < 32)
			{
				ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwForceAttack), &IsShooting, sizeof(int), 0);
				
				if (IsShooting != 5)
				{
					/*Sleep(200);*/
					doattack = 5;
					WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwForceAttack), &doattack, sizeof(int), 0);
				}
				//mouse_event(MOUSEEVENTF_LEFTDOWN, NULL, NULL, NULL, NULL);
			}
			else
			{
				ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwForceAttack), &IsShooting, sizeof(int), 0);

				if (IsShooting != 4)
				{
					doattack = 4;
					WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwForceAttack), &doattack, sizeof(int), 0);
				}
				//mouse_event(MOUSEEVENTF_LEFTUP, NULL, NULL, NULL, NULL);
			}
		/*}*/
		Sleep(1);
	}
}
