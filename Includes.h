#ifndef INCLUDES_H //Define Guard
#define INCLUDES_H
#define WIN32_LEAN_AND_MEAN //Excludes Headers We Wont Use (Increase Compile Time)

#include <Windows.h> //Standard Windows Functions/Data Types
#include <algorithm>
#include <iostream> //Constains Input/Output Functions (cin/cout etc..)
#include <TlHelp32.h> //Contains Read/Write Functions
#include <string> //Support For Strings
#include <sstream> //Supports Data Conversion
#include <cstdint>
#include "Hprocess.h"
#include "Offsets.h"

CHackProcess fProcess;

#endif