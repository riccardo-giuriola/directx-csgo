#pragma once

#include "Includes.h"

struct PlayerDataChams
{
	DWORD PlayerBase;
	int Team;

	void readPlayerInfo()
	{
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwLocalPlayer), &PlayerBase, sizeof(DWORD), NULL);
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(PlayerBase + hazedumper::netvars::m_iTeamNum), &Team, sizeof(int), NULL);
	}

}PlayerDataChams;

struct EntityDataChams
{
	DWORD EntityBase;
	int Team;
	int r;
	int g;
	int b;
	int a = 255;

	void writeMemory(int i)
	{
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwEntityList + (i * 0x10)), &EntityBase, sizeof(DWORD), NULL);
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(EntityBase + hazedumper::netvars::m_iTeamNum), &Team, sizeof(int), NULL);
		if (Team == PlayerDataChams.Team)
		{
			r = ChamTeamColorR;
			g = ChamTeamColorG;
			b = ChamTeamColorB;
		}
		else
		{
			r = ChamEnemyColorR;
			g = ChamEnemyColorG;
			b = ChamEnemyColorB;
		}
		WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(EntityBase + hazedumper::netvars::m_clrRender), &r, sizeof(int), NULL);
		WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(EntityBase + hazedumper::netvars::m_clrRender + 0x1), &g, sizeof(int), NULL);
		WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(EntityBase + hazedumper::netvars::m_clrRender + 0x2), &b, sizeof(int), NULL);
		WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(EntityBase + hazedumper::netvars::m_clrRender + 0x3), &a, sizeof(int), NULL);
	}

}EntityDataChams[NumOfPlayers];

void runChams()
{
	while (true)
	{
		PlayerDataChams.readPlayerInfo();

		for (int i = 0; i < NumOfPlayers; i++)
		{
			EntityDataChams[i].writeMemory(i);
		}
		Sleep(1);
	}
}