#pragma once
#include "Offsets.h"
#include "Includes.h"

//HFONT Font;

typedef struct
{
	float flMatrix[4][4];
}WorldToScreenMatrix_t;

float Get3dDistance(float * myCoords, float * enemyCoords)
{
	return sqrt(
		pow(double(enemyCoords[0] - myCoords[0]), 2.0) +
		pow(double(enemyCoords[1] - myCoords[1]), 2.0) +
		pow(double(enemyCoords[2] - myCoords[2]), 2.0));
}

struct MyPlayer_t
{
	DWORD CLocalPlayer;
	int Team;
	int Health;
	int IsScoping;
	int FOV;
	WorldToScreenMatrix_t WorldToScreenMatrix;
	float Position[3];
	int flickerCheck;
	void ReadInformation()
	{
		// Reading CLocalPlayer Pointer to our "CLocalPlayer" DWORD.
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwLocalPlayer), &CLocalPlayer, sizeof(DWORD), 0);
		// Reading out our Team to our "Team" Varible.
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(CLocalPlayer + hazedumper::netvars::m_iTeamNum), &Team, sizeof(int), 0);
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(CLocalPlayer + hazedumper::netvars::m_iHealth), &Health, sizeof(int), 0);
		// Reading out our Position to our "Position" Varible.
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(CLocalPlayer + hazedumper::netvars::m_vecOrigin), &Position, sizeof(float[3]), 0);

		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(CLocalPlayer + hazedumper::netvars::m_bIsScoped), &IsScoping, sizeof(int), 0);

		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(CLocalPlayer + hazedumper::netvars::m_iFOVStart), &FOV, sizeof(int), 0);

		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwViewMatrix), &WorldToScreenMatrix, sizeof(WorldToScreenMatrix), 0);
	}
}MyPlayer;


//ENemy struct
struct PlayerList_t
{
	DWORD CBaseEntity;
	int Team;
	int Health;
	int fFlags;
	int state;
	float Position[3];
	float AimbotAngle[3];

	void ReadInformation(int Player)
	{
		// Reading CBaseEntity Pointer to our "CBaseEntity" DWORD + Current Player in the loop. 0x10 is the CBaseEntity List Size
		//"client.dll"+00545204 //0x571A5204
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwEntityList + (Player * 0x10)), &CBaseEntity, sizeof(DWORD), 0);
		// Reading out our Team to our "Team" Varible.
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(CBaseEntity + hazedumper::netvars::m_iTeamNum), &Team, sizeof(int), 0);
		// Reading out our Health to our "Health" Varible. 
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(CBaseEntity + hazedumper::netvars::m_iHealth), &Health, sizeof(int), 0);
		// Reading out our Position to our "Position" Varible.
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(CBaseEntity + hazedumper::netvars::m_vecOrigin), &Position, sizeof(float[3]), 0);

		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(CBaseEntity + hazedumper::netvars::m_iState), &state, sizeof(int), 0);

		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(CBaseEntity + hazedumper::netvars::m_fFlags), &fFlags, sizeof(int), 0);
	}
}PlayerList[NumOfPlayers];


bool WorldToScreen(float * from, float * to)
{
	float w = 0.0f;

	to[0] = MyPlayer.WorldToScreenMatrix.flMatrix[0][0] * from[0] + MyPlayer.WorldToScreenMatrix.flMatrix[0][1] * from[1] + MyPlayer.WorldToScreenMatrix.flMatrix[0][2] * from[2] + MyPlayer.WorldToScreenMatrix.flMatrix[0][3];
	to[1] = MyPlayer.WorldToScreenMatrix.flMatrix[1][0] * from[0] + MyPlayer.WorldToScreenMatrix.flMatrix[1][1] * from[1] + MyPlayer.WorldToScreenMatrix.flMatrix[1][2] * from[2] + MyPlayer.WorldToScreenMatrix.flMatrix[1][3];
	w = MyPlayer.WorldToScreenMatrix.flMatrix[3][0] * from[0] + MyPlayer.WorldToScreenMatrix.flMatrix[3][1] * from[1] + MyPlayer.WorldToScreenMatrix.flMatrix[3][2] * from[2] + MyPlayer.WorldToScreenMatrix.flMatrix[3][3];

	if (w < 0.01f)
		return false;

	float invw = 1.0f / w;
	to[0] *= invw;
	to[1] *= invw;

	int width = (int)(m_Rect.right - m_Rect.left);
	int height = (int)(m_Rect.bottom - m_Rect.top);

	float x = width / 2;
	float y = height / 2;

	x += 0.5 * to[0] * width + 0.5;
	y -= 0.5 * to[1] * height + 0.5;

	to[0] = x + m_Rect.left;
	to[1] = y + m_Rect.top;

	return true;
}

void DrawESP(int x, int y, float distance, int i, int r, int g, int b)
{
	if (MyPlayer.FOV == 40 && MyPlayer.IsScoping == 1)
	{
		distance /= 3;
	}
	if (MyPlayer.FOV == 15 && MyPlayer.IsScoping == 1)
	{
		distance /= 8;
	}
	if (MyPlayer.FOV == 10 && MyPlayer.IsScoping == 1)
	{
		distance /= 10;
	}
	//ESP RECTANGLE
	int width = 22000 / distance;
	int height = 48000 / distance;
	DrawBorderBox(x - (width / 2), y - height, width, height, 1, 255, r, g, b, distance, PlayerList[i].Health);
}

void ESP()
{
	if (SeeESP)
	{
		MyPlayer.ReadInformation();

		/*if (MyPlayer.Health > 0)
		{*/
			for (int i = 0; i < NumOfPlayers; i++)
			{
				PlayerList[i].ReadInformation(i);

				if (PlayerList[i].state == 1 || PlayerList[i].state == 2)
					continue;

				if (PlayerList[i].Health < 1 || PlayerList[i].Health > 100)
					continue;

				if (EntityData[i].fFlags == 257 || PlayerList[i].fFlags == 0 || PlayerList[i].fFlags == 256 || EntityData[i].fFlags == 263 || PlayerList[i].fFlags == 261)
					continue;

				float TeamXY[3];
				if (PlayerList[i].Team == MyPlayer.Team)
				{
					if (WorldToScreen(PlayerList[i].Position, TeamXY))
					{
						DrawESP(TeamXY[0] - m_Rect.left, TeamXY[1] - m_Rect.top, Get3dDistance(MyPlayer.Position, PlayerList[i].Position), i, ESPTeamColorR, ESPTeamColorG, ESPTeamColorB);
					}
				}

				float EnemyXY[3];
				if (PlayerList[i].Team != MyPlayer.Team)
				{
					if (WorldToScreen(PlayerList[i].Position, EnemyXY))
					{
						DrawESP(EnemyXY[0] - m_Rect.left, EnemyXY[1] - m_Rect.top, Get3dDistance(MyPlayer.Position, PlayerList[i].Position), i, ESPEnemyColorR, ESPEnemyColorG, ESPEnemyColorB);

						/*ID3DXFont* pFont;
						D3DXCreateFont(p_Device, 50, 0, FW_NORMAL, 1, false, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Arial", &pFont);

						std::stringstream ss;
						ss << (float)EntityList[i].classid;

						char * distanceInfo = new char[ss.str().size() + 1];
						strcpy(distanceInfo, ss.str().c_str());

						DrawString(distanceInfo, 100, 100, 255, 255, 0, pFont);

						delete[] distanceInfo;*/
					}
				}
				/*PlayerList[i].Position[0] = NULL;
				PlayerList[i].Position[1] = NULL;
				PlayerList[i].Position[2] = NULL;*/
			}
		/*}*/
	}
}