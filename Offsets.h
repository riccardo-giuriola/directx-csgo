#pragma once
#include <iostream>
#include <cstdint>

// 2019-05-03 11:36:50.128586200 UTC

namespace hazedumper {
	constexpr ::std::int64_t timestamp = 1556883410;
	namespace netvars {
		constexpr ::std::ptrdiff_t cs_gamerules_data = 0x0;
		constexpr ::std::ptrdiff_t m_ArmorValue = 0xB340;
		constexpr ::std::ptrdiff_t m_Collision = 0x31C;
		constexpr ::std::ptrdiff_t m_CollisionGroup = 0x474;
		constexpr ::std::ptrdiff_t m_Local = 0x2FBC;
		constexpr ::std::ptrdiff_t m_MoveType = 0x25C;
		constexpr ::std::ptrdiff_t m_OriginalOwnerXuidHigh = 0x31B4;
		constexpr ::std::ptrdiff_t m_OriginalOwnerXuidLow = 0x31B0;
		constexpr ::std::ptrdiff_t m_SurvivalGameRuleDecisionTypes = 0x1320;
		constexpr ::std::ptrdiff_t m_SurvivalRules = 0xCF8;
		constexpr ::std::ptrdiff_t m_aimPunchAngle = 0x302C;
		constexpr ::std::ptrdiff_t m_aimPunchAngleVel = 0x3038;
		constexpr ::std::ptrdiff_t m_angEyeAnglesX = 0xB344;
		constexpr ::std::ptrdiff_t m_angEyeAnglesY = 0xB348;
		constexpr ::std::ptrdiff_t m_bBombPlanted = 0x99D;
		constexpr ::std::ptrdiff_t m_bFreezePeriod = 0x20;
		constexpr ::std::ptrdiff_t m_bGunGameImmunity = 0x392C;
		constexpr ::std::ptrdiff_t m_bHasDefuser = 0xB350;
		constexpr ::std::ptrdiff_t m_bHasHelmet = 0xB334;
		constexpr ::std::ptrdiff_t m_bInReload = 0x3285;
		constexpr ::std::ptrdiff_t m_bIsDefusing = 0x3918;
		constexpr ::std::ptrdiff_t m_bIsQueuedMatchmaking = 0x74;
		constexpr ::std::ptrdiff_t m_bIsScoped = 0x3910;
		constexpr ::std::ptrdiff_t m_bIsValveDS = 0x75;
		constexpr ::std::ptrdiff_t m_bSpotted = 0x93D;
		constexpr ::std::ptrdiff_t m_bSpottedByMask = 0x980;
		constexpr ::std::ptrdiff_t m_bStartedArming = 0x33D0;
		constexpr ::std::ptrdiff_t m_clrRender = 0x70;
		constexpr ::std::ptrdiff_t m_dwBoneMatrix = 0x26A8;
		constexpr ::std::ptrdiff_t m_fAccuracyPenalty = 0x3310;
		constexpr ::std::ptrdiff_t m_fFlags = 0x104;
		constexpr ::std::ptrdiff_t m_flC4Blow = 0x2990;
		constexpr ::std::ptrdiff_t m_flDefuseCountDown = 0x29AC;
		constexpr ::std::ptrdiff_t m_flDefuseLength = 0x29A8;
		constexpr ::std::ptrdiff_t m_flFallbackWear = 0x31C0;
		constexpr ::std::ptrdiff_t m_flFlashDuration = 0xA3F4;
		constexpr ::std::ptrdiff_t m_flFlashMaxAlpha = 0xA3F0;
		constexpr ::std::ptrdiff_t m_flLastBoneSetupTime = 0x2924;
		constexpr ::std::ptrdiff_t m_flLowerBodyYawTarget = 0x3A78;
		constexpr ::std::ptrdiff_t m_flNextAttack = 0x2D70;
		constexpr ::std::ptrdiff_t m_flNextPrimaryAttack = 0x3218;
		constexpr ::std::ptrdiff_t m_flSimulationTime = 0x268;
		constexpr ::std::ptrdiff_t m_flTimerLength = 0x2994;
		constexpr ::std::ptrdiff_t m_hActiveWeapon = 0x2EF8;
		constexpr ::std::ptrdiff_t m_hMyWeapons = 0x2DF8;
		constexpr ::std::ptrdiff_t m_hObserverTarget = 0x3388;
		constexpr ::std::ptrdiff_t m_hOwner = 0x29CC;
		constexpr ::std::ptrdiff_t m_hOwnerEntity = 0x14C;
		constexpr ::std::ptrdiff_t m_iAccountID = 0x2FC8;
		constexpr ::std::ptrdiff_t m_iClip1 = 0x3244;
		constexpr ::std::ptrdiff_t m_iCompetitiveRanking = 0x1A84;
		constexpr ::std::ptrdiff_t m_iCompetitiveWins = 0x1B88;
		constexpr ::std::ptrdiff_t m_iCrosshairId = 0xB3AC;
		constexpr ::std::ptrdiff_t m_iEntityQuality = 0x2FAC;
		constexpr ::std::ptrdiff_t m_iFOV = 0x31E4;
		constexpr ::std::ptrdiff_t m_iFOVStart = 0x31E8;
		constexpr ::std::ptrdiff_t m_iGlowIndex = 0xA40C;
		constexpr ::std::ptrdiff_t m_iHealth = 0x100;
		constexpr ::std::ptrdiff_t m_iItemDefinitionIndex = 0x2FAA;
		constexpr ::std::ptrdiff_t m_iItemIDHigh = 0x2FC0;
		constexpr ::std::ptrdiff_t m_iMostRecentModelBoneCounter = 0x2690;
		constexpr ::std::ptrdiff_t m_iObserverMode = 0x3374;
		constexpr ::std::ptrdiff_t m_iShotsFired = 0xA380;
		constexpr ::std::ptrdiff_t m_iState = 0x3238;
		constexpr ::std::ptrdiff_t m_iTeamNum = 0xF4;
		constexpr ::std::ptrdiff_t m_lifeState = 0x25F;
		constexpr ::std::ptrdiff_t m_nFallbackPaintKit = 0x31B8;
		constexpr ::std::ptrdiff_t m_nFallbackSeed = 0x31BC;
		constexpr ::std::ptrdiff_t m_nFallbackStatTrak = 0x31C4;
		constexpr ::std::ptrdiff_t m_nForceBone = 0x268C;
		constexpr ::std::ptrdiff_t m_nTickBase = 0x342C;
		constexpr ::std::ptrdiff_t m_rgflCoordinateFrame = 0x444;
		constexpr ::std::ptrdiff_t m_szCustomName = 0x303C;
		constexpr ::std::ptrdiff_t m_szLastPlaceName = 0x35B0;
		constexpr ::std::ptrdiff_t m_thirdPersonViewAngles = 0x31D8;
		constexpr ::std::ptrdiff_t m_vecOrigin = 0x138;
		constexpr ::std::ptrdiff_t m_vecVelocity = 0x114;
		constexpr ::std::ptrdiff_t m_vecViewOffset = 0x108;
		constexpr ::std::ptrdiff_t m_viewPunchAngle = 0x3020;
	} // namespace netvars
	namespace signatures {
		constexpr ::std::ptrdiff_t clientstate_choked_commands = 0x4D28;
		constexpr ::std::ptrdiff_t clientstate_delta_ticks = 0x174;
		constexpr ::std::ptrdiff_t clientstate_last_outgoing_command = 0x4D24;
		constexpr ::std::ptrdiff_t clientstate_net_channel = 0x9C;
		constexpr ::std::ptrdiff_t convar_name_hash_table = 0x2F0F8;
		constexpr ::std::ptrdiff_t dwClientState = 0x58BCFC;
		constexpr ::std::ptrdiff_t dwClientState_GetLocalPlayer = 0x180;
		constexpr ::std::ptrdiff_t dwClientState_IsHLTV = 0x4D40;
		constexpr ::std::ptrdiff_t dwClientState_Map = 0x28C;
		constexpr ::std::ptrdiff_t dwClientState_MapDirectory = 0x188;
		constexpr ::std::ptrdiff_t dwClientState_MaxPlayer = 0x388;
		constexpr ::std::ptrdiff_t dwClientState_PlayerInfo = 0x52B8;
		constexpr ::std::ptrdiff_t dwClientState_State = 0x108;
		constexpr ::std::ptrdiff_t dwClientState_ViewAngles = 0x4D88;
		constexpr ::std::ptrdiff_t dwEntityList = 0x4CFD35C;
		constexpr ::std::ptrdiff_t dwForceAttack = 0x312EA2C;
		constexpr ::std::ptrdiff_t dwForceAttack2 = 0x312EA38;
		constexpr ::std::ptrdiff_t dwForceBackward = 0x312EA80;
		constexpr ::std::ptrdiff_t dwForceForward = 0x312EA8C;
		constexpr ::std::ptrdiff_t dwForceJump = 0x51A08C4;
		constexpr ::std::ptrdiff_t dwForceLeft = 0x312EA08;
		constexpr ::std::ptrdiff_t dwForceRight = 0x312E9FC;
		constexpr ::std::ptrdiff_t dwGameDir = 0x631F70;
		constexpr ::std::ptrdiff_t dwGameRulesProxy = 0x5212C0C;
		constexpr ::std::ptrdiff_t dwGetAllClasses = 0xD105DC;
		constexpr ::std::ptrdiff_t dwGlobalVars = 0x58BA00;
		constexpr ::std::ptrdiff_t dwGlowObjectManager = 0x523D718;
		constexpr ::std::ptrdiff_t dwInput = 0x51483A8;
		constexpr ::std::ptrdiff_t dwInterfaceLinkList = 0x8C2584;
		constexpr ::std::ptrdiff_t dwLocalPlayer = 0xCEB95C;
		constexpr ::std::ptrdiff_t dwMouseEnable = 0xCF14A8;
		constexpr ::std::ptrdiff_t dwMouseEnablePtr = 0xCF1478;
		constexpr ::std::ptrdiff_t dwPlayerResource = 0x312CDAC;
		constexpr ::std::ptrdiff_t dwRadarBase = 0x51320BC;
		constexpr ::std::ptrdiff_t dwSensitivity = 0xCF1344;
		constexpr ::std::ptrdiff_t dwSensitivityPtr = 0xCF1318;
		constexpr ::std::ptrdiff_t dwSetClanTag = 0x896A0;
		constexpr ::std::ptrdiff_t dwViewMatrix = 0x4CEED74;
		constexpr ::std::ptrdiff_t dwWeaponTable = 0x5148E6C;
		constexpr ::std::ptrdiff_t dwWeaponTableIndex = 0x323C;
		constexpr ::std::ptrdiff_t dwYawPtr = 0xCF1108;
		constexpr ::std::ptrdiff_t dwZoomSensitivityRatioPtr = 0xCF6348;
		constexpr ::std::ptrdiff_t dwbSendPackets = 0xD277A;
		constexpr ::std::ptrdiff_t dwppDirect3DDevice9 = 0xA6030;
		constexpr ::std::ptrdiff_t force_update_spectator_glow = 0x391F72;
		constexpr ::std::ptrdiff_t interface_engine_cvar = 0x3E9EC;
		constexpr ::std::ptrdiff_t is_c4_owner = 0x39E2B0;
		constexpr ::std::ptrdiff_t m_bDormant = 0xED;
		constexpr ::std::ptrdiff_t m_pStudioHdr = 0x294C;
		constexpr ::std::ptrdiff_t m_pitchClassPtr = 0x5132368;
		constexpr ::std::ptrdiff_t m_yawClassPtr = 0xCF1108;
		constexpr ::std::ptrdiff_t model_ambient_min = 0x58ED1C;
		constexpr ::std::ptrdiff_t set_abs_angles = 0x1C9670;
		constexpr ::std::ptrdiff_t set_abs_origin = 0x1C94B0;
	} // namespace signatures
} // namespace hazedumper

bool ScriptsMenu = true;
bool VisualsMenu = false;
bool OptionsMenu = false;
//
int menux = GetSystemMetrics(SM_CXSCREEN) / 3;
int menuy = GetSystemMetrics(SM_CYSCREEN) / 6;
//
int MenuR = 255;
int MenuG = 0;
int MenuB = 0;
int AccentR = 17;
int AccentG = 17;
int AccentB = 17;
//
bool Aimbot = false;
bool Trigger = false;
bool bhop = false;
bool ESPBox = false;
bool GlowESPEnabled = false;
bool ChamsEnabled = false;
bool SlowAimEnabled = false;
bool NoFlashEnabled = false;
bool RadarEnabled = false;
bool Head = true;
bool Neck = false;
bool Chest = false;
bool Stomach = false;
//
bool RainbowMenu = false;
bool RainbowCrosshair = false;
//
int IdleCrossR = 237;
int IdleCrossG = 184;
int IdleCrossB = 26;
//
int SpottedCrossR = 26;
int SpottedCrossG = 184;
int SpottedCrossB = 237;
//
int SpeedBarX = 40;
int ActivationBarX = 10;
//
bool SeeMenu = true;
bool SeeESP = false;
//
int const NumOfPlayers = 32;
int BoneNumber = 8;
int smooth = 20;
int triggerDistance = 5;
//
int ESPTeamColorR = 0;
int ESPTeamColorG = 255;
int ESPTeamColorB = 0;

int ESPEnemyColorR = 255;
int ESPEnemyColorG = 0;
int ESPEnemyColorB = 0;

int GlowTeamColorR = 0;
int GlowTeamColorG = 255;
int GlowTeamColorB = 0;

int GlowEnemyColorR = 255;
int GlowEnemyColorG = 0;
int GlowEnemyColorB = 0;

int ChamTeamColorR = 0;
int ChamTeamColorG = 255;
int ChamTeamColorB = 0;

int ChamEnemyColorR = 255;
int ChamEnemyColorG = 0;
int ChamEnemyColorB = 0;
//
bool ESPRedEnemy = true;
bool ESPGreenEnemy = false;
bool ESPBlueEnemy = false;
bool ESPPurpleEnemy = false;
bool ESPYellowEnemy = false;
bool ESPWhiteEnemy = false;

bool ESPRedTeam = false;
bool ESPGreenTeam = true;
bool ESPBlueTeam = false;
bool ESPPurpleTeam = false;
bool ESPYellowTeam = false;
bool ESPWhiteTeam = false;

bool GlowRedEnemy = true;
bool GlowGreenEnemy = false;
bool GlowBlueEnemy = false;
bool GlowPurpleEnemy = false;
bool GlowYellowEnemy = false;
bool GlowWhiteEnemy = false;

bool GlowRedTeam = false;
bool GlowGreenTeam = true;
bool GlowBlueTeam = false;
bool GlowPurpleTeam = false;
bool GlowYellowTeam = false;
bool GlowWhiteTeam = false;

bool ChamRedEnemy = true;
bool ChamGreenEnemy = false;
bool ChamBlueEnemy = false;
bool ChamPurpleEnemy = false;
bool ChamYellowEnemy = false;
bool ChamWhiteEnemy = false;

bool ChamRedTeam = false;
bool ChamGreenTeam = true;
bool ChamBlueTeam = false;
bool ChamPurpleTeam = false;
bool ChamYellowTeam = false;
bool ChamWhiteTeam = false;
//
//Menu colors
bool MenuColorRed = false;
bool MenuColorOrange = false;
bool MenuColorYellow = false;
bool MenuColorLime = false;
bool MenuColorGreen = false;
bool MenuColorCyan = false;
bool MenuColorBlue = false;
bool MenuColorPurple = false;
bool MenuColorMagenta = false;
bool MenuColorPink = false;
bool MenuColorBrown = false;
bool MenuColorNavy = false;
//
HANDLE abThread = 0;
HANDLE tbThread = 0;
HANDLE bhThread = 0;
HANDLE rdThread = 0;
HANDLE nfThread = 0;
HANDLE ghThread = 0;
HANDLE chThread = 0;
//
RECT m_Rect;