#include "Includes.h"

using namespace std;

DWORD ClientState;

int diffx, diffy;
float newX, newY;

struct Vector
{
	float x, y, z;
};

Vector Angle2Aim;

struct Player
{
	DWORD PlayerBase;
	Vector Position;
	Vector ViewAngles;
	int Team;

	void readMemory()
	{
		//Reading PlayerBase
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwLocalPlayer), &PlayerBase, sizeof(DWORD), 0);
		//Reading player position
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(PlayerBase + hazedumper::netvars::m_vecOrigin), &Position, sizeof(Vector), 0);
		//Reading my team number
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(PlayerBase + hazedumper::netvars::m_iTeamNum), &Team, sizeof(int), 0);
		//Reading ClientState base offset
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordEngine + hazedumper::signatures::dwClientState), &ClientState, sizeof(DWORD), 0);
		//Reading player view angles X, Y, Z
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(ClientState + hazedumper::signatures::dwClientState_ViewAngles), &ViewAngles, sizeof(Vector), 0);
	}
}PlayerData;

struct Entity
{
	DWORD dwEntityBase;
	DWORD BoneMatrix;
	Vector BonePosition;
	int spotted;
	int Health;
	int team;
	int fFlags;

	void readMemory(int i)
	{
		//Reading EntityBase offset
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwEntityList + (i * 0x10)), &dwEntityBase, sizeof(DWORD), NULL);
		//Reading entities bone matrix base offset
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(dwEntityBase + hazedumper::netvars::m_dwBoneMatrix), &BoneMatrix, sizeof(DWORD), NULL);
		//Reading X position of bone
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(BoneMatrix + 0x30 * BoneNumber + 0x0C), &BonePosition.x, sizeof(Vector), NULL);
		//Reading Y position of bone
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(BoneMatrix + 0x30 * BoneNumber + 0x1C), &BonePosition.y, sizeof(Vector), NULL);
		//Reading Z position of bone
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(BoneMatrix + 0x30 * BoneNumber + 0x2C), &BonePosition.z, sizeof(Vector), NULL);
		//Reading if the entity is visible
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(dwEntityBase + hazedumper::netvars::m_bSpottedByMask), &spotted, sizeof(int), NULL);
		//Reading the entity's health
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(dwEntityBase + hazedumper::netvars::m_iHealth), &Health, sizeof(int), NULL);
		//Reading the entity' team
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(dwEntityBase + hazedumper::netvars::m_iTeamNum), &team, sizeof(int), NULL);
		//Reading the entity fFlag
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(dwEntityBase + hazedumper::netvars::m_fFlags), &fFlags, sizeof(int), NULL);
	}

}EntityData[NumOfPlayers];

float Get3D(float X, float Y, float Z, float eX, float eY, float eZ)
{
	return(sqrtf((eX - X) * (eX - X) + (eY - Y) * (eY - Y) + (eZ - Z) * (eZ - Z)));
}

float CloseEnt()
{
	//Variables
	float fLowest = 100000, TMP;
	int iIndex = 0;

	for (int i = 0; i < NumOfPlayers; i++)
	{
		//Store Distances In Array
		TMP = Get3D(PlayerData.Position.x, PlayerData.Position.y, PlayerData.Position.z, EntityData[i].BonePosition.x, EntityData[i].BonePosition.y, EntityData[i].BonePosition.z);

		//If Enemy Has Lower Distance The Player 1, Replace (var)Lowest With Current Enemy Distance
		if (TMP < fLowest && EntityData[i].Health > 0 && EntityData[i].team != PlayerData.Team && EntityData[i].spotted > 0)
		{
			fLowest = TMP;
			iIndex = i;
		}
	}
	return iIndex;
}

Vector calangle(Vector src, Vector dir)
{
	Vector angle;
	Vector Newangle;
	angle.x = dir.x - src.x;
	angle.y = dir.y - src.y;
	angle.z = dir.z - src.z;

	float Magnitude = sqrt(angle.x * angle.x + angle.y * angle.y + angle.z * angle.z);
	Newangle.x = -atan2(angle.z - 64.0f, Magnitude) * 180 / 3.14;
	Newangle.y = atan2(angle.y, angle.x) * 180 / 3.14;
	Newangle.z = 0;
	return Newangle;
}

void runAimbot()
{
	fProcess.RunProcess();

	while (true)
	{
		if (GetAsyncKeyState(0x58))
		{
			PlayerData.readMemory();
			for (int i = 0; i < NumOfPlayers; i++)
			{
				EntityData[i].readMemory(i);
			}

			int index = CloseEnt();
			Angle2Aim = calangle(PlayerData.Position, EntityData[index].BonePosition);
			diffx = abs(Angle2Aim.x - PlayerData.ViewAngles.x);
			diffy = abs(Angle2Aim.y - PlayerData.ViewAngles.y);
			if (diffx < triggerDistance && diffy < triggerDistance)
			{

				if (/*EntityData[index].fFlags == 257 || */EntityData[index].fFlags == 0 || EntityData[index].fFlags == 256 || /*EntityData[index].fFlags == 263 || */EntityData[index].fFlags == 261)
					continue;

				newX = PlayerData.ViewAngles.x;
				newY = PlayerData.ViewAngles.y;
				for (int i = 0; i < smooth; i++)
				{
					newX += (Angle2Aim.x - PlayerData.ViewAngles.x) / smooth;
					newY += (Angle2Aim.y - PlayerData.ViewAngles.y) / smooth;
					//Writing X to player view angle
					WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(ClientState + hazedumper::signatures::dwClientState_ViewAngles), &newX, sizeof(float), 0);
					//Writing Y to player view angle
					WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(ClientState + hazedumper::signatures::dwClientState_ViewAngles + 0x4), &newY, sizeof(float), 0);
				}
			}
		}
		Sleep(1);
	}
}