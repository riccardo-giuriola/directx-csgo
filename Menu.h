﻿#include <Windows.h>
#include <thread>
#include <iostream>
#include <conio.h>
#include "Offsets.h"
#include "Aimbot.h"
#include "Triggerbot.h"
#include "Bhop.h"
#include "Radar.h"
#include "NoFlash.h"
#include "GlowESP.h"
#include "Chams.h"

void DrawPages();
void RainbowFeatures();
void Menu();

//char strSpeed[32];
//char strDistance[32];

//bool MouseEnabled = false;
//bool MouseDisabled = true;

void Menu()
{
	if (!SeeMenu)
	{
		/*WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwMouseEnable), &MouseDisabled, sizeof(bool), NULL);*/
		/*Mem.Write<bool>(Client + Mouse, true);*/
	}
	if (SeeMenu)
	{
		/*WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwMouseEnable), &MouseEnabled, sizeof(bool), NULL);*/
		/*Mem.Write<bool>(Client + Mouse, false);*/

		// Footer
		FillRGB(menux , menuy + 355, 600, 20, 8, 8, 8, 255);
		// Tab Base
		FillRGB(menux , menuy - 40, 600, 40, 8, 8, 8, 230);
		DrawGUIBox(menux, menuy - 75, 600, 450, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		FillRGB(menux + 1, menuy - 75, 599, 35, MenuR, MenuG, MenuB, 230);
		DrawString("CS:GO Private Menu", menux + 210, menuy - 65, 255, 255, 255, pFontLarge);
		DrawLine(menux, menuy + 355, menux + 600, menuy + 355, MenuR, MenuG, MenuB, 255);

		DrawString("www.cheatsbazar.com", menux + 12, menuy + 356, 255, 255, 255, pFontSmall);
		//DrawString("Version 0.7.4", menux + 510, menuy + 356, 255, 255, 255, pFontSmall);
		DrawString("Version 1.0.0", menux + 460, menuy + 356, 255, 255, 255, pFontSmall);
		DrawPages();
		DrawString("Scripts", menux + 60, menuy - 30, 255, 255, 255, pFontLarge);
		DrawString("Visuals", menux + 260, menuy - 30, 255, 255, 255, pFontLarge);
		DrawString("Options", menux + 460, menuy - 30, 255, 255, 255, pFontLarge);
		//DrawLine(menux + 1, menuy - 1, menux + 600, menuy - 1, 255, 255, 255, 230);
		DrawLine(menux + 1, menuy - 40, menux + 600, menuy - 40, 255, 255, 255, 230);
		POINT p;
		GetCursorPos(&p);
		//if (GetCursorPos(&p))
		//{
		//	//FillRGB(p.x, p.y, 9, 9, MenuR, MenuG, MenuB, 255);
		//	DrawCursorLine(p.x, p.y, p.x + 6, p.y + 20, 255, 255, 255, 255);
		//	DrawCursorLine(p.x, p.y, p.x + 20, p.y + 6, 255, 255, 255, 255);
		//	if ((GetAsyncKeyState(VK_LBUTTON)))
		//	{
		//		DrawCursorLine(p.x, p.y, p.x + 6, p.y + 20, MenuR, MenuG, MenuB, 255);
		//		DrawCursorLine(p.x, p.y, p.x + 20, p.y + 6, MenuR, MenuG, MenuB, 255);
		//	}
		//}
		// Change Tabs
		if ((GetAsyncKeyState(VK_LBUTTON)))
		{
			if ((p.x > menux + 1 && p.x < menux + 199) && (p.y > menuy - 40 && p.y < menuy))
			{
				ScriptsMenu = true;
				VisualsMenu = false;
				OptionsMenu = false;
			}
			if ((p.x > menux + 200 && p.x < menux + 399) && (p.y > menuy - 40 && p.y < menuy))
			{
				VisualsMenu = true;
				ScriptsMenu = false;
				OptionsMenu = false;
			}
			if ((p.x > menux + 400 && p.x < menux + 599) && (p.y > menuy - 40 && p.y < menuy))
			{
				OptionsMenu = true;
				ScriptsMenu = false;
				VisualsMenu = false;
			}
			if ((p.x > menux && p.x < menux + 600) && (p.y > menuy - 80 && p.y < menuy - 40))
			{
				menux = p.x - 275;
				menuy = p.y + 60;
			}
		}
	}
	Sleep(1);
}
void DrawPages()
{
	if (ScriptsMenu)
	{
		POINT p;
		GetCursorPos(&p);
		// Toggle Features
		if ((GetAsyncKeyState(VK_LBUTTON) & 1))
		{
			//Aimbot box
			if ((p.x > menux + 20 && p.x < menux + 35) && (p.y > menuy + 20 && p.y < menuy + 35))
			{
				Aimbot = !Aimbot;
			}
			//Trigger box
			if ((p.x > menux + 20 && p.x < menux + 35) && (p.y > menuy + 60 && p.y < menuy + 75))
			{
				Trigger = !Trigger;
			}
			//Bhop box
			if ((p.x > menux + 20 && p.x < menux + 35) && (p.y > menuy + 100 && p.y < menuy + 115))
			{
				bhop = !bhop;
			}
			//Radar box
			if ((p.x > menux + 20 && p.x < menux + 35) && (p.y > menuy + 140 && p.y < menuy + 155))
			{
				RadarEnabled = !RadarEnabled;
			}
			//No flash box
			if ((p.x > menux + 20 && p.x < menux + 35) && (p.y > menuy + 180 && p.y < menuy + 195))
			{
				NoFlashEnabled = !NoFlashEnabled;
			}
			//Aim to Head
			if ((p.x > menux + 20 && p.x < menux + 170) && (p.y > menuy + 235 && p.y < menuy + 260))
			{
				Head = !Head;
				BoneNumber = 8;
				Neck = false;
				Chest = false;
				Stomach = false;
			}
			//Aim to Neck
			if ((p.x > menux + 20 && p.x < menux + 170) && (p.y > menuy + 260 && p.y < menuy + 285))
			{
				Neck = !Neck;
				BoneNumber = 7;
				Head = false;
				Chest = false;
				Stomach = false;
			}
			//Aim to Chest
			if ((p.x > menux + 20 && p.x < menux + 170) && (p.y > menuy + 285 && p.y < menuy + 310))
			{
				Chest = !Chest;
				BoneNumber = 6;
				Head = false;
				Neck = false;
				Stomach = false;
			}
			//Aim to Stomach
			if ((p.x > menux + 20 && p.x < menux + 170) && (p.y > menuy + 310 && p.y < menuy + 335))
			{
				Stomach = !Stomach;
				BoneNumber = 3;
				Head = false;
				Neck = false;
				Chest = false;
			}
			//SpeedBar subtract
			if ((p.x > menux + 300 && p.x < menux + 400) && (p.y > menuy + 100 && p.y < menuy + 125))
			{
				if (smooth >= 10)
				{
					SpeedBarX -= 10;
					smooth -= 5;
				}
			}
			//Speedbar add
			if ((p.x > menux + 400 && p.x < menux + 500) && (p.y > menuy + 100 && p.y < menuy + 125))
			{
				if (SpeedBarX != 200)
				{
					SpeedBarX += 10;
					smooth += 5;
				}
			}
			//Activationbar subtract
			if ((p.x > menux + 300 && p.x < menux + 400) && (p.y > menuy + 200 && p.y < menuy + 225))
			{
				if (triggerDistance >= 10)
				{
					ActivationBarX -= 10;
					triggerDistance -= 5;
				}
			}
			//Activationbar add
			if ((p.x > menux + 400 && p.x < menux + 500) && (p.y > menuy + 200 && p.y < menuy + 225))
			{
				if (ActivationBarX != 200)
				{
					ActivationBarX += 10;
					triggerDistance += 5;
				}
			}
		}

		DrawGUIBox(menux + 1, menuy - 40, 200, 39, 255, 255, 255, 230, MenuR, MenuG, MenuB, 230);
		FillRGB(menux + 202, menuy - 40, 199, 40, MenuR, MenuG, MenuB, 150);
		FillRGB(menux + 401, menuy - 40, 200, 40, MenuR, MenuG, MenuB, 150);

		/*DrawGUIBox(menux + 20, menuy + 75, 150, 100, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);*/

		/*itoa(smooth, strSpeed, 10);
		itoa(triggerDistance, strDistance, 10);*/

		FillRGB(menux + 20, menuy + 210, 151, 25, MenuR, MenuG, MenuB, 230);
		DrawString("Target:", menux + 25, menuy + 212, 255, 255, 255, pFontSmall);

		DrawString("Aiming speed:", menux + 300, menuy + 50, 255, 255, 255, pFontSmall);
		DrawGUIBox(menux + 300, menuy + 80, 200, 5, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		FillRGB(menux + 300 + SpeedBarX, menuy + 73, 10, 20, MenuR, MenuG, MenuB, 255);
		FillRGB(menux + 300, menuy + 100, 100, 25, MenuR, MenuG, MenuB, 230);
		DrawGUIBox(menux + 300, menuy + 100, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		DrawString("[-]", menux + 337, menuy + 103, 255, 255, 255, pFontSmall);
		FillRGB(menux + 400, menuy + 100, 100, 25, MenuR, MenuG, MenuB, 230);
		DrawGUIBox(menux + 400, menuy + 100, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		DrawString("[+]", menux + 437, menuy + 103, 255, 255, 255, pFontSmall);
		/*DrawString(strSpeed, menux + 512, menuy + 73, 255, 255, 255, pFontSmall);*/

		DrawString("Activation distance:", menux + 300, menuy + 150, 255, 255, 255, pFontSmall);
		DrawGUIBox(menux + 300, menuy + 180, 200, 5, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		FillRGB(menux + 300 + ActivationBarX, menuy + 173, 10, 20, MenuR, MenuG, MenuB, 255);
		FillRGB(menux + 300, menuy + 200, 100, 25, MenuR, MenuG, MenuB, 230);
		DrawGUIBox(menux + 300, menuy + 200, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		DrawString("[-]", menux + 337, menuy + 203, 255, 255, 255, pFontSmall);
		FillRGB(menux + 400, menuy + 200, 100, 25, MenuR, MenuG, MenuB, 230);
		DrawGUIBox(menux + 400, menuy + 200, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		DrawString("[+]", menux + 437, menuy + 203, 255, 255, 255, pFontSmall);
		/*DrawString(strDistance, menux + 512, menuy + 173, 255, 255, 255, pFontSmall);*/

			
			if (Aimbot)
			{
				FillRGB(menux + 20, menuy + 20, 15, 15, MenuR, MenuG, MenuB, 230);
				DrawString("Aimbot", menux + 40, menuy + 18, 255, 255, 255, pFontSmall);
				if (abThread == 0)
				{
					abThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)runAimbot, 0, 0, 0);
				}
			}
			if (!Aimbot)
			{
				DrawGUIBox(menux + 20, menuy + 20, 14, 14, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
				DrawString("Aimbot", menux + 40, menuy + 18, 255, 255, 255, pFontSmall);
				if (abThread != 0)
				{
					TerminateThread(abThread, 0);
					abThread = 0;
				}
			}

			if (Trigger)
			{
				FillRGB(menux + 20, menuy + 60, 15, 15, MenuR, MenuG, MenuB, 230);
				DrawString("Auto Shoot", menux + 40, menuy + 58, 255, 255, 255, pFontSmall);
				if (tbThread == 0)
				{
					tbThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)runTrigger, 0, 0, 0);
				}
			}
			if (!Trigger)
			{
				DrawGUIBox(menux + 20, menuy + 60, 14, 14, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
				DrawString("Auto Shoot", menux + 40, menuy + 58, 255, 255, 255, pFontSmall);
				if (tbThread != 0)
				{
					TerminateThread(tbThread, 0);
					tbThread = 0;
				}
			}

			if (bhop)
			{
				FillRGB(menux + 20, menuy + 100, 15, 15, MenuR, MenuG, MenuB, 230);
				DrawString("Bunny Hop", menux + 40, menuy + 98, 255, 255, 255, pFontSmall);
				if (bhThread == 0)
				{
					bhThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)runBhop, 0, 0, 0);
				}
			}
			if (!bhop)
			{
				DrawGUIBox(menux + 20, menuy + 100, 14, 14, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
				DrawString("Bunny Hop", menux + 40, menuy + 98, 255, 255, 255, pFontSmall);
				if (bhThread != 0)
				{
					TerminateThread(bhThread, 0);
					bhThread = 0;
				}
			}

			if (RadarEnabled)
			{
				FillRGB(menux + 20, menuy + 140, 15, 15, MenuR, MenuG, MenuB, 230);
				DrawString("Radar", menux + 40, menuy + 138, 255, 255, 255, pFontSmall);
				if (rdThread == 0)
				{
					rdThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)runRadar, 0, 0, 0);
				}
			}
			if (!RadarEnabled)
			{
				DrawGUIBox(menux + 20, menuy + 140, 14, 14, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
				DrawString("Radar", menux + 40, menuy + 138, 255, 255, 255, pFontSmall);
				if (rdThread != 0)
				{
					TerminateThread(rdThread, 0);
					rdThread = 0;
				}
			}

			if (NoFlashEnabled)
			{
				FillRGB(menux + 20, menuy + 180, 15, 15, MenuR, MenuG, MenuB, 230);
				DrawString("Remove Flashbang", menux + 40, menuy + 178, 255, 255, 255, pFontSmall);
				if (nfThread == 0)
				{
					nfThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)runNoFlash, 0, 0, 0);
				}
			}
			if (!NoFlashEnabled)
			{
				DrawGUIBox(menux + 20, menuy + 180, 14, 14, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
				DrawString("Remove Flashbang", menux + 40, menuy + 178, 255, 255, 255, pFontSmall);
				if (nfThread != 0)
				{
					TerminateThread(nfThread, 0);
					nfThread = 0;
				}
			}

			if (Head)
			{
				FillRGB(menux + 20, menuy + 235, 150, 25, MenuR, MenuG, MenuB, 100);
				DrawString("Head", menux + 74, menuy + 238, 255, 255, 255, pFontSmall);
			}
			if (!Head)
			{
				DrawGUIBox(menux + 20, menuy + 235, 150, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
				DrawString("Head", menux + 74, menuy + 238, 255, 255, 255, pFontSmall);
			}

			if (Neck)
			{
				FillRGB(menux + 20, menuy + 260, 150, 25, MenuR, MenuG, MenuB, 100);
				DrawString("Neck", menux + 74, menuy + 263, 255, 255, 255, pFontSmall);
			}
			if (!Neck)
			{
				DrawGUIBox(menux + 20, menuy + 260, 150, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
				DrawString("Neck", menux + 74, menuy + 263, 255, 255, 255, pFontSmall);
			}

			if (Chest)
			{
				FillRGB(menux + 20, menuy + 285, 150, 25, MenuR, MenuG, MenuB, 100);
				DrawString("Chest", menux + 70, menuy + 288, 255, 255, 255, pFontSmall);
			}
			if (!Chest)
			{
				DrawGUIBox(menux + 20, menuy + 285, 150, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
				DrawString("Chest", menux + 70, menuy + 288, 255, 255, 255, pFontSmall);
			}

			if (Stomach)
			{
				FillRGB(menux + 20, menuy + 310, 150, 25, MenuR, MenuG, MenuB, 100);
				DrawString("Stomach", menux + 64, menuy + 313, 255, 255, 255, pFontSmall);
			}
			if (!Stomach)
			{
				DrawGUIBox(menux + 20, menuy + 310, 150, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
				DrawString("Stomach", menux + 64, menuy + 313, 255, 255, 255, pFontSmall);
			}
	}
	if (VisualsMenu)
	{
		POINT p;
		GetCursorPos(&p);
		// Toggle Features
		if ((GetAsyncKeyState(VK_LBUTTON) & 1))
		{
			//ESP Box
			if ((p.x > menux + 20 && p.x < menux + 35) && (p.y > menuy + 20 && p.y < menuy + 35))
			{
				ESPBox = !ESPBox;
			}
			//Glow ESP
			if ((p.x > menux + 20 && p.x < menux + 35) && (p.y > menuy + 60 && p.y < menuy + 75))
			{
				GlowESPEnabled = !GlowESPEnabled;
			}
			//Chams
			if ((p.x > menux + 20 && p.x < menux + 35) && (p.y > menuy + 100 && p.y < menuy + 115))
			{
				ChamsEnabled = !ChamsEnabled;
			}

			//ESP Red Enemy
			if ((p.x > menux + 20 && p.x < menux + 110) && (p.y > menuy + 165 && p.y < menuy + 190))
			{
				ESPRedEnemy = true;
				ESPEnemyColorR = 255;
				ESPEnemyColorG = 0;
				ESPEnemyColorB = 0;
				ESPGreenEnemy = false;
				ESPBlueEnemy = false;
				ESPPurpleEnemy = false;
				ESPYellowEnemy = false;
				ESPWhiteEnemy = false;
			}
			//ESP Green Enemy
			if ((p.x > menux + 110 && p.x < menux + 200) && (p.y > menuy + 165 && p.y < menuy + 190))
			{
				ESPGreenEnemy = true;
				ESPEnemyColorR = 0;
				ESPEnemyColorG = 255;
				ESPEnemyColorB = 0;
				ESPRedEnemy = false;
				ESPBlueEnemy = false;
				ESPPurpleEnemy = false;
				ESPYellowEnemy = false;
				ESPWhiteEnemy = false;
			}
			//ESP Blue Enemy
			if ((p.x > menux + 20 && p.x < menux + 110) && (p.y > menuy + 190 && p.y < menuy + 215))
			{
				ESPBlueEnemy = true;
				ESPEnemyColorR = 0;
				ESPEnemyColorG = 0;
				ESPEnemyColorB = 255;
				ESPRedEnemy = false;
				ESPGreenEnemy = false;
				ESPPurpleEnemy = false;
				ESPYellowEnemy = false;
				ESPWhiteEnemy = false;
			}
			//ESP Purple Enemy
			if ((p.x > menux + 110 && p.x < menux + 200) && (p.y > menuy + 190 && p.y < menuy + 215))
			{
				ESPPurpleEnemy = true;
				ESPEnemyColorR = 128;
				ESPEnemyColorG = 0;
				ESPEnemyColorB = 128;
				ESPRedEnemy = false;
				ESPGreenEnemy = false;
				ESPBlueEnemy = false;
				ESPYellowEnemy = false;
				ESPWhiteEnemy = false;
			}
			//ESP Yellow Enemy
			if ((p.x > menux + 20 && p.x < menux + 110) && (p.y > menuy + 215 && p.y < menuy + 240))
			{
				ESPYellowEnemy = true;
				ESPEnemyColorR = 255;
				ESPEnemyColorG = 224;
				ESPEnemyColorB = 32;
				ESPRedEnemy = false;
				ESPGreenEnemy = false;
				ESPBlueEnemy = false;
				ESPPurpleEnemy = false;
				ESPWhiteEnemy = false;
			}
			//ESP White Enemy
			if ((p.x > menux + 110 && p.x < menux + 200) && (p.y > menuy + 215 && p.y < menuy + 240))
			{
				ESPWhiteEnemy = true;
				ESPEnemyColorR = 255;
				ESPEnemyColorG = 255;
				ESPEnemyColorB = 255;
				ESPRedEnemy = false;
				ESPGreenEnemy = false;
				ESPBlueEnemy = false;
				ESPPurpleEnemy = false;
				ESPYellowEnemy = false;
			}

			//ESP Red Team
			if ((p.x > menux + 20 && p.x < menux + 110) && (p.y > menuy + 265 && p.y < menuy + 290))
			{
				ESPRedTeam = true;
				ESPTeamColorR = 255;
				ESPTeamColorG = 0;
				ESPTeamColorB = 0;
				ESPGreenTeam = false;
				ESPBlueTeam = false;
				ESPPurpleTeam = false;
				ESPYellowTeam = false;
				ESPWhiteTeam = false;
			}
			//ESP Green Team
			if ((p.x > menux + 110 && p.x < menux + 200) && (p.y > menuy + 265 && p.y < menuy + 290))
			{
				ESPGreenTeam = true;
				ESPTeamColorR = 0;
				ESPTeamColorG = 255;
				ESPTeamColorB = 0;
				ESPRedTeam = false;
				ESPBlueTeam = false;
				ESPPurpleTeam = false;
				ESPYellowTeam = false;
				ESPWhiteTeam = false;
			}
			//ESP Blue Team
			if ((p.x > menux + 20 && p.x < menux + 110) && (p.y > menuy + 290 && p.y < menuy + 315))
			{
				ESPBlueTeam = true;
				ESPTeamColorR = 0;
				ESPTeamColorG = 0;
				ESPTeamColorB = 255;
				ESPRedTeam = false;
				ESPGreenTeam = false;
				ESPPurpleTeam = false;
				ESPYellowTeam = false;
				ESPWhiteTeam = false;
			}
			//ESP Purple Team
			if ((p.x > menux + 110 && p.x < menux + 200) && (p.y > menuy + 290 && p.y < menuy + 315))
			{
				ESPPurpleTeam = true;
				ESPTeamColorR = 128;
				ESPTeamColorG = 0;
				ESPTeamColorB = 128;
				ESPRedTeam = false;
				ESPGreenTeam = false;
				ESPBlueTeam = false;
				ESPYellowTeam = false;
				ESPWhiteTeam = false;
			}
			//ESP Yellow Team
			if ((p.x > menux + 20 && p.x < menux + 110) && (p.y > menuy + 315 && p.y < menuy + 340))
			{
				ESPYellowTeam = true;
				ESPTeamColorR = 255;
				ESPTeamColorG = 224;
				ESPTeamColorB = 32;
				ESPRedTeam = false;
				ESPGreenTeam = false;
				ESPBlueTeam = false;
				ESPPurpleTeam = false;
				ESPWhiteTeam = false;
			}
			//ESP White Team
			if ((p.x > menux + 110 && p.x < menux + 200) && (p.y > menuy + 315 && p.y < menuy + 340))
			{
				ESPWhiteTeam = true;
				ESPTeamColorR = 255;
				ESPTeamColorG = 255;
				ESPTeamColorB = 255;
				ESPRedTeam = false;
				ESPGreenTeam = false;
				ESPBlueTeam = false;
				ESPPurpleTeam = false;
				ESPYellowTeam = false;
			}

			//Glow Red Enemy
			if ((p.x > menux + 210 && p.x < menux + 300) && (p.y > menuy + 165 && p.y < menuy + 190))
			{
				GlowRedEnemy = true;
				GlowEnemyColorR = 255;
				GlowEnemyColorG = 0;
				GlowEnemyColorB = 0;
				GlowGreenEnemy = false;
				GlowBlueEnemy = false;
				GlowPurpleEnemy = false;
				GlowYellowEnemy = false;
				GlowWhiteEnemy = false;
			}
			//Glow Green Enemy
			if ((p.x > menux + 300 && p.x < menux + 390) && (p.y > menuy + 165 && p.y < menuy + 190))
			{
				GlowGreenEnemy = true;
				GlowEnemyColorR = 0;
				GlowEnemyColorG = 255;
				GlowEnemyColorB = 0;
				GlowRedEnemy = false;
				GlowBlueEnemy = false;
				GlowPurpleEnemy = false;
				GlowYellowEnemy = false;
				GlowWhiteEnemy = false;
			}
			//Glow Blue Enemy
			if ((p.x > menux + 210 && p.x < menux + 300) && (p.y > menuy + 190 && p.y < menuy + 215))
			{
				GlowBlueEnemy = true;
				GlowEnemyColorR = 0;
				GlowEnemyColorG = 0;
				GlowEnemyColorB = 255;
				GlowRedEnemy = false;
				GlowGreenEnemy = false;
				GlowPurpleEnemy = false;
				GlowYellowEnemy = false;
				GlowWhiteEnemy = false;
			}
			//Glow Purple Enemy
			if ((p.x > menux + 300 && p.x < menux + 390) && (p.y > menuy + 190 && p.y < menuy + 215))
			{
				GlowPurpleEnemy = true;
				GlowEnemyColorR = 128;
				GlowEnemyColorG = 0;
				GlowEnemyColorB = 128;
				GlowRedEnemy = false;
				GlowGreenEnemy = false;
				GlowBlueEnemy = false;
				GlowYellowEnemy = false;
				GlowWhiteEnemy = false;
			}
			//Glow Yellow Enemy
			if ((p.x > menux + 210 && p.x < menux + 300) && (p.y > menuy + 215 && p.y < menuy + 240))
			{
				GlowYellowEnemy = true;
				GlowEnemyColorR = 255;
				GlowEnemyColorG = 255;
				GlowEnemyColorB = 0;
				GlowRedEnemy = false;
				GlowGreenEnemy = false;
				GlowBlueEnemy = false;
				GlowPurpleEnemy = false;
				GlowWhiteEnemy = false;
			}
			//Glow White Enemy
			if ((p.x > menux + 300 && p.x < menux + 390) && (p.y > menuy + 215 && p.y < menuy + 240))
			{
				GlowWhiteEnemy = true;
				GlowEnemyColorR = 255;
				GlowEnemyColorG = 255;
				GlowEnemyColorB = 255;
				GlowRedEnemy = false;
				GlowGreenEnemy = false;
				GlowBlueEnemy = false;
				GlowPurpleEnemy = false;
				GlowYellowEnemy = false;
			}

			//Glow Red Team
			if ((p.x > menux + 210 && p.x < menux + 300) && (p.y > menuy + 265 && p.y < menuy + 290))
			{
				GlowRedTeam = true;
				GlowTeamColorR = 255;
				GlowTeamColorG = 0;
				GlowTeamColorB = 0;
				GlowGreenTeam = false;
				GlowBlueTeam = false;
				GlowPurpleTeam = false;
				GlowYellowTeam = false;
				GlowWhiteTeam = false;
			}
			//Glow Green Team
			if ((p.x > menux + 300 && p.x < menux + 390) && (p.y > menuy + 265 && p.y < menuy + 290))
			{
				GlowGreenTeam = true;
				GlowTeamColorR = 0;
				GlowTeamColorG = 255;
				GlowTeamColorB = 0;
				GlowRedTeam = false;
				GlowBlueTeam = false;
				GlowPurpleTeam = false;
				GlowYellowTeam = false;
				GlowWhiteTeam = false;
			}
			//Glow Blue Team
			if ((p.x > menux + 210 && p.x < menux + 300) && (p.y > menuy + 290 && p.y < menuy + 315))
			{
				GlowBlueTeam = true;
				GlowTeamColorR = 0;
				GlowTeamColorG = 0;
				GlowTeamColorB = 255;
				GlowRedTeam = false;
				GlowGreenTeam = false;
				GlowPurpleTeam = false;
				GlowYellowTeam = false;
				GlowWhiteTeam = false;
			}
			//Glow Purple Team
			if ((p.x > menux + 300 && p.x < menux + 390) && (p.y > menuy + 290 && p.y < menuy + 315))
			{
				GlowPurpleTeam = true;
				GlowTeamColorR = 128;
				GlowTeamColorG = 0;
				GlowTeamColorB = 128;
				GlowRedTeam = false;
				GlowGreenTeam = false;
				GlowBlueTeam = false;
				GlowYellowTeam = false;
				GlowWhiteTeam = false;
			}
			//Glow Yellow Team
			if ((p.x > menux + 210 && p.x < menux + 300) && (p.y > menuy + 315 && p.y < menuy + 340))
			{
				GlowYellowTeam = true;
				GlowTeamColorR = 255;
				GlowTeamColorG = 255;
				GlowTeamColorB = 0;
				GlowRedTeam = false;
				GlowGreenTeam = false;
				GlowBlueTeam = false;
				GlowPurpleTeam = false;
				GlowWhiteTeam = false;
			}
			//Glow White Team
			if ((p.x > menux + 300 && p.x < menux + 390) && (p.y > menuy + 315 && p.y < menuy + 340))
			{
				GlowWhiteTeam = true;
				GlowTeamColorR = 255;
				GlowTeamColorG = 255;
				GlowTeamColorB = 255;
				GlowRedTeam = false;
				GlowGreenTeam = false;
				GlowBlueTeam = false;
				GlowPurpleTeam = false;
				GlowYellowTeam = false;
			}

			//Cham Red Enemy
			if ((p.x > menux + 400 && p.x < menux + 490) && (p.y > menuy + 165 && p.y < menuy + 190))
			{
				ChamRedEnemy = true;
				ChamEnemyColorR = 255;
				ChamEnemyColorG = 0;
				ChamEnemyColorB = 0;
				ChamGreenEnemy = false;
				ChamBlueEnemy = false;
				ChamPurpleEnemy = false;
				ChamYellowEnemy = false;
				ChamWhiteEnemy = false;
			}
			//Cham Green Enemy
			if ((p.x > menux + 490 && p.x < menux + 580) && (p.y > menuy + 165 && p.y < menuy + 190))
			{
				ChamGreenEnemy = true;
				ChamEnemyColorR = 0;
				ChamEnemyColorG = 255;
				ChamEnemyColorB = 0;
				ChamRedEnemy = false;
				ChamBlueEnemy = false;
				ChamPurpleEnemy = false;
				ChamYellowEnemy = false;
				ChamWhiteEnemy = false;
			}
			//Cham Blue Enemy
			if ((p.x > menux + 400 && p.x < menux + 490) && (p.y > menuy + 190 && p.y < menuy + 215))
			{
				ChamBlueEnemy = true;
				ChamEnemyColorR = 0;
				ChamEnemyColorG = 0;
				ChamEnemyColorB = 255;
				ChamRedEnemy = false;
				ChamGreenEnemy = false;
				ChamPurpleEnemy = false;
				ChamYellowEnemy = false;
				ChamWhiteEnemy = false;
			}
			//Cham Purple Enemy
			if ((p.x > menux + 490 && p.x < menux + 580) && (p.y > menuy + 190 && p.y < menuy + 215))
			{
				ChamPurpleEnemy = true;
				ChamEnemyColorR = 128;
				ChamEnemyColorG = 0;
				ChamEnemyColorB = 128;
				ChamRedEnemy = false;
				ChamGreenEnemy = false;
				ChamBlueEnemy = false;
				ChamYellowEnemy = false;
				ChamWhiteEnemy = false;
			}
			//Cham Yellow Enemy
			if ((p.x > menux + 400 && p.x < menux + 490) && (p.y > menuy + 215 && p.y < menuy + 240))
			{
				ChamYellowEnemy = true;
				ChamEnemyColorR = 255;
				ChamEnemyColorG = 224;
				ChamEnemyColorB = 32;
				ChamRedEnemy = false;
				ChamGreenEnemy = false;
				ChamBlueEnemy = false;
				ChamPurpleEnemy = false;
				ChamWhiteEnemy = false;
			}
			//Cham White Enemy
			if ((p.x > menux + 490 && p.x < menux + 580) && (p.y > menuy + 215 && p.y < menuy + 240))
			{
				ChamWhiteEnemy = true;
				ChamEnemyColorR = 255;
				ChamEnemyColorG = 255;
				ChamEnemyColorB = 255;
				ChamRedEnemy = false;
				ChamGreenEnemy = false;
				ChamBlueEnemy = false;
				ChamPurpleEnemy = false;
				ChamYellowEnemy = false;
			}

			//Cham Red Team
			if ((p.x > menux + 400 && p.x < menux + 490) && (p.y > menuy + 265 && p.y < menuy + 290))
			{
				ChamRedTeam = true;
				ChamTeamColorR = 255;
				ChamTeamColorG = 0;
				ChamTeamColorB = 0;
				ChamGreenTeam = false;
				ChamBlueTeam = false;
				ChamPurpleTeam = false;
				ChamYellowTeam = false;
				ChamWhiteTeam = false;
			}
			//Cham Green Team
			if ((p.x > menux + 490 && p.x < menux + 580) && (p.y > menuy + 265 && p.y < menuy + 290))
			{
				ChamGreenTeam = true;
				ChamTeamColorR = 0;
				ChamTeamColorG = 255;
				ChamTeamColorB = 0;
				ChamRedTeam = false;
				ChamBlueTeam = false;
				ChamPurpleTeam = false;
				ChamYellowTeam = false;
				ChamWhiteTeam = false;
			}
			//Cham Blue Team
			if ((p.x > menux + 400 && p.x < menux + 490) && (p.y > menuy + 290 && p.y < menuy + 315))
			{
				ChamBlueTeam = true;
				ChamTeamColorR = 0;
				ChamTeamColorG = 0;
				ChamTeamColorB = 255;
				ChamRedTeam = false;
				ChamGreenTeam = false;
				ChamPurpleTeam = false;
				ChamYellowTeam = false;
				ChamWhiteTeam = false;
			}
			//Cham Purple Team
			if ((p.x > menux + 490 && p.x < menux + 580) && (p.y > menuy + 290 && p.y < menuy + 315))
			{
				ChamPurpleTeam = true;
				ChamTeamColorR = 128;
				ChamTeamColorG = 0;
				ChamTeamColorB = 128;
				ChamRedTeam = false;
				ChamGreenTeam = false;
				ChamBlueTeam = false;
				ChamYellowTeam = false;
				ChamWhiteTeam = false;
			}
			//Cham Yellow Team
			if ((p.x > menux + 400 && p.x < menux + 490) && (p.y > menuy + 315 && p.y < menuy + 340))
			{
				ChamYellowTeam = true;
				ChamTeamColorR = 255;
				ChamTeamColorG = 224;
				ChamTeamColorB = 32;
				ChamRedTeam = false;
				ChamGreenTeam = false;
				ChamBlueTeam = false;
				ChamPurpleTeam = false;
				ChamWhiteTeam = false;
			}
			//Cham White Team
			if ((p.x > menux + 490 && p.x < menux + 580) && (p.y > menuy + 315 && p.y < menuy + 340))
			{
				ChamWhiteTeam = true;
				ChamTeamColorR = 255;
				ChamTeamColorG = 255;
				ChamTeamColorB = 255;
				ChamRedTeam = false;
				ChamGreenTeam = false;
				ChamBlueTeam = false;
				ChamPurpleTeam = false;
				ChamYellowTeam = false;
			}
		}
		FillRGB(menux + 1, menuy - 40, 200, 40, MenuR, MenuG, MenuB, 150);
		DrawGUIBox(menux + 201, menuy - 40, 199, 39, 255, 255, 255, 230, MenuR, MenuG, MenuB, 230);
		FillRGB(menux + 401, menuy - 40, 200, 40, MenuR, MenuG, MenuB, 150);

		//ESP menu title
		DrawGUIBox(menux + 20, menuy + 140, 180, 200, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		FillRGB(menux + 20, menuy + 140, 180, 25, MenuR, MenuG, MenuB, 230);
		DrawString("ESP Color Enemy", menux + 26, menuy + 142, 255, 255, 255, pFontSmall);
		FillRGB(menux + 20, menuy + 240, 180, 25, MenuR, MenuG, MenuB, 230);
		DrawString("ESP Color Team", menux + 24, menuy + 246, 255, 255, 255, pFontSmall);

		//Glow menu title
		DrawGUIBox(menux + 210, menuy + 140, 180, 200, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		FillRGB(menux + 210, menuy + 140, 180, 25, MenuR, MenuG, MenuB, 230);
		DrawString("Glow Color Enemy", menux + 216, menuy + 142, 255, 255, 255, pFontSmall);
		FillRGB(menux + 210, menuy + 240, 180, 25, MenuR, MenuG, MenuB, 230);
		DrawString("Glow Color Team", menux + 214, menuy + 246, 255, 255, 255, pFontSmall);

		//Glow menu title
		DrawGUIBox(menux + 400, menuy + 140, 180, 200, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		FillRGB(menux + 400, menuy + 140, 180, 25, MenuR, MenuG, MenuB, 230);
		DrawString("Chams Color Enemy", menux + 406, menuy + 142, 255, 255, 255, pFontSmall);
		FillRGB(menux + 400, menuy + 240, 180, 25, MenuR, MenuG, MenuB, 230);
		DrawString("Chams Color Team", menux + 404, menuy + 246, 255, 255, 255, pFontSmall);

		//ESP box
		if (ESPBox)
		{
			FillRGB(menux + 20, menuy + 20, 15, 15, MenuR, MenuG, MenuB, 230);
			DrawString("ESP Box", menux + 40, menuy + 18, 255, 255, 255, pFontSmall);
			SeeESP = true;
		}
		if (!ESPBox)
		{
			DrawGUIBox(menux + 20, menuy + 20, 14, 14, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("ESP Box", menux + 40, menuy + 18, 255, 255, 255, pFontSmall);
			SeeESP = false;
		}

		//Glow box
		if (GlowESPEnabled)
		{
			FillRGB(menux + 20, menuy + 60, 15, 15, MenuR, MenuG, MenuB, 230);
			DrawString("Glow ESP", menux + 40, menuy + 58, 255, 255, 255, pFontSmall);
			if (ghThread == 0)
			{
				ghThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)runGlowESP, 0, 0, 0);
			}
		}
		if (!GlowESPEnabled)
		{
			DrawGUIBox(menux + 20, menuy + 60, 14, 14, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Glow ESP", menux + 40, menuy + 58, 255, 255, 255, pFontSmall);
			if (ghThread != 0)
			{
				TerminateThread(ghThread, 0);
				ghThread = 0;
			}
		}

		//Chams box
		if (ChamsEnabled)
		{
			FillRGB(menux + 20, menuy + 100, 15, 15, MenuR, MenuG, MenuB, 230);
			DrawString("Chams", menux + 40, menuy + 98, 255, 255, 255, pFontSmall);
			if (chThread == 0)
			{
				chThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)runChams, 0, 0, 0);
			}
		}
		if (!ChamsEnabled)
		{
			DrawGUIBox(menux + 20, menuy + 100, 14, 14, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Chams", menux + 40, menuy + 98, 255, 255, 255, pFontSmall);
			if (chThread != 0)
			{
				TerminateThread(chThread, 0);
				chThread = 0;
			}
		}

		//ESP colors menu enemy
		if (ESPRedEnemy)
		{
			FillRGB(menux + 21, menuy + 166, 89, 24, 255, 0, 0, 255);
			DrawString("Red", menux + 25, menuy + 168, 255, 255, 255, pFontSmall);
		}
		if (!ESPRedEnemy)
		{
			DrawGUIBox(menux + 20, menuy + 165, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Red", menux + 25, menuy + 168, 255, 255, 255, pFontSmall);
		}

		if (ESPGreenEnemy)
		{
			FillRGB(menux + 111, menuy + 166, 89, 24, 0, 255, 0, 255);
			DrawString("Green", menux + 115, menuy + 168, 255, 255, 255, pFontSmall);
		}
		if (!ESPGreenEnemy)
		{
			DrawGUIBox(menux + 110, menuy + 165, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Green", menux + 115, menuy + 168, 255, 255, 255, pFontSmall);
		}

		if (ESPBlueEnemy)
		{
			FillRGB(menux + 21, menuy + 191, 89, 24, 0, 0, 255, 255);
			DrawString("Blue", menux + 25, menuy + 193, 255, 255, 255, pFontSmall);
		}
		if (!ESPBlueEnemy)
		{
			DrawGUIBox(menux + 20, menuy + 190, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Blue", menux + 25, menuy + 193, 255, 255, 255, pFontSmall);
		}

		if (ESPPurpleEnemy)
		{
			FillRGB(menux + 111, menuy + 191, 89, 24, 128, 0, 128, 255);
			DrawString("Purple", menux + 115, menuy + 193, 255, 255, 255, pFontSmall);
		}
		if (!ESPPurpleEnemy)
		{
			DrawGUIBox(menux + 110, menuy + 190, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Purple", menux + 115, menuy + 193, 255, 255, 255, pFontSmall);
		}

		if (ESPYellowEnemy)
		{
			FillRGB(menux + 21, menuy + 216, 89, 24, 255, 224, 32, 255);
			DrawString("Yellow", menux + 25, menuy + 218, 255, 255, 255, pFontSmall);
		}
		if (!ESPYellowEnemy)
		{
			DrawGUIBox(menux + 20, menuy + 215, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Yellow", menux + 25, menuy + 218, 255, 255, 255, pFontSmall);
		}
		if (ESPWhiteEnemy)
		{
			FillRGB(menux + 111, menuy + 216, 89, 24, 255, 255, 255, 255);
			DrawString("White", menux + 115, menuy + 218, 0, 0, 0, pFontSmall);
		}
		if (!ESPWhiteEnemy)
		{
			DrawGUIBox(menux + 110, menuy + 215, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("White", menux + 115, menuy + 218, 255, 255, 255, pFontSmall);
		}

		//Glow colors menu enemy
		if (GlowRedEnemy)
		{
			FillRGB(menux + 211, menuy + 166, 89, 24, 255, 0, 0, 255);
			DrawString("Red", menux + 215, menuy + 168, 255, 255, 255, pFontSmall);
		}
		if (!GlowRedEnemy)
		{
			DrawGUIBox(menux + 210, menuy + 165, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Red", menux + 215, menuy + 168, 255, 255, 255, pFontSmall);
		}

		if (GlowGreenEnemy)
		{
			FillRGB(menux + 301, menuy + 166, 89, 24, 0, 255, 0, 255);
			DrawString("Green", menux + 305, menuy + 168, 255, 255, 255, pFontSmall);
		}
		if (!GlowGreenEnemy)
		{
			DrawGUIBox(menux + 300, menuy + 165, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Green", menux + 305, menuy + 168, 255, 255, 255, pFontSmall);
		}

		if (GlowBlueEnemy)
		{
			FillRGB(menux + 211, menuy + 191, 89, 24, 0, 0, 255, 255);
			DrawString("Blue", menux + 215, menuy + 193, 255, 255, 255, pFontSmall);
		}
		if (!GlowBlueEnemy)
		{
			DrawGUIBox(menux + 210, menuy + 190, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Blue", menux + 215, menuy + 193, 255, 255, 255, pFontSmall);
		}

		if (GlowPurpleEnemy)
		{
			FillRGB(menux + 301, menuy + 191, 89, 24, 128, 0, 128, 255);
			DrawString("Purple", menux + 305, menuy + 193, 255, 255, 255, pFontSmall);
		}
		if (!GlowPurpleEnemy)
		{
			DrawGUIBox(menux + 300, menuy + 190, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Purple", menux + 305, menuy + 193, 255, 255, 255, pFontSmall);
		}

		if (GlowYellowEnemy)
		{
			FillRGB(menux + 211, menuy + 216, 89, 24, 255, 224, 32, 255);
			DrawString("Yellow", menux + 215, menuy + 218, 255, 255, 255, pFontSmall);
		}
		if (!GlowYellowEnemy)
		{
			DrawGUIBox(menux + 210, menuy + 215, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Yellow", menux + 215, menuy + 218, 255, 255, 255, pFontSmall);
		}
		if (GlowWhiteEnemy)
		{
			FillRGB(menux + 301, menuy + 216, 89, 24, 255, 255, 255, 255);
			DrawString("White", menux + 305, menuy + 218, 0, 0, 0, pFontSmall);
		}
		if (!GlowWhiteEnemy)
		{
			DrawGUIBox(menux + 300, menuy + 215, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("White", menux + 305, menuy + 218, 255, 255, 255, pFontSmall);
		}

		//Chams colors menu enemy
		if (ChamRedEnemy)
		{
			FillRGB(menux + 401, menuy + 166, 89, 24, 255, 0, 0, 255);
			DrawString("Red", menux + 405, menuy + 168, 255, 255, 255, pFontSmall);
		}
		if (!ChamRedEnemy)
		{
			DrawGUIBox(menux + 400, menuy + 165, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Red", menux + 405, menuy + 168, 255, 255, 255, pFontSmall);
		}

		if (ChamGreenEnemy)
		{
			FillRGB(menux + 491, menuy + 166, 89, 24, 0, 255, 0, 255);
			DrawString("Green", menux + 495, menuy + 168, 255, 255, 255, pFontSmall);
		}
		if (!ChamGreenEnemy)
		{
			DrawGUIBox(menux + 490, menuy + 165, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Green", menux + 495, menuy + 168, 255, 255, 255, pFontSmall);
		}

		if (ChamBlueEnemy)
		{
			FillRGB(menux + 401, menuy + 191, 89, 24, 0, 0, 255, 255);
			DrawString("Blue", menux + 405, menuy + 193, 255, 255, 255, pFontSmall);
		}
		if (!ChamBlueEnemy)
		{
			DrawGUIBox(menux + 400, menuy + 190, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Blue", menux + 405, menuy + 193, 255, 255, 255, pFontSmall);
		}

		if (ChamPurpleEnemy)
		{
			FillRGB(menux + 491, menuy + 191, 89, 24, 128, 0, 128, 255);
			DrawString("Purple", menux + 495, menuy + 193, 255, 255, 255, pFontSmall);
		}
		if (!ChamPurpleEnemy)
		{
			DrawGUIBox(menux + 490, menuy + 190, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Purple", menux + 495, menuy + 193, 255, 255, 255, pFontSmall);
		}

		if (ChamYellowEnemy)
		{
			FillRGB(menux + 401, menuy + 216, 89, 24, 255, 224, 32, 255);
			DrawString("Yellow", menux + 405, menuy + 218, 255, 255, 255, pFontSmall);
		}
		if (!ChamYellowEnemy)
		{
			DrawGUIBox(menux + 400, menuy + 215, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Yellow", menux + 405, menuy + 218, 255, 255, 255, pFontSmall);
		}
		if (ChamWhiteEnemy)
		{
			FillRGB(menux + 491, menuy + 216, 89, 24, 255, 255, 255, 255);
			DrawString("White", menux + 495, menuy + 218, 0, 0, 0, pFontSmall);
		}
		if (!ChamWhiteEnemy)
		{
			DrawGUIBox(menux + 490, menuy + 215, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("White", menux + 495, menuy + 218, 255, 255, 255, pFontSmall);
		}

		//ESP colors menu team
		if (ESPRedTeam)
		{
			FillRGB(menux + 21, menuy + 266, 89, 24, 255, 0, 0, 255);
			DrawString("Red", menux + 25, menuy + 268, 255, 255, 255, pFontSmall);
		}
		if (!ESPRedTeam)
		{
			DrawGUIBox(menux + 20, menuy + 265, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Red", menux + 25, menuy + 268, 255, 255, 255, pFontSmall);
		}

		if (ESPGreenTeam)
		{
			FillRGB(menux + 111, menuy + 266, 89, 24, 0, 255, 0, 255);
			DrawString("Green", menux + 115, menuy + 268, 255, 255, 255, pFontSmall);
		}
		if (!ESPGreenTeam)
		{
			DrawGUIBox(menux + 110, menuy + 265, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Green", menux + 115, menuy + 268, 255, 255, 255, pFontSmall);
		}

		if (ESPBlueTeam)
		{
			FillRGB(menux + 21, menuy + 291, 89, 24, 0, 0, 255, 255);
			DrawString("Blue", menux + 25, menuy + 293, 255, 255, 255, pFontSmall);
		}
		if (!ESPBlueTeam)
		{
			DrawGUIBox(menux + 20, menuy + 290, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Blue", menux + 25, menuy + 293, 255, 255, 255, pFontSmall);
		}

		if (ESPPurpleTeam)
		{
			FillRGB(menux + 111, menuy + 291, 89, 24, 128, 0, 128, 255);
			DrawString("Purple", menux + 115, menuy + 293, 255, 255, 255, pFontSmall);
		}
		if (!ESPPurpleTeam)
		{
			DrawGUIBox(menux + 110, menuy + 290, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Purple", menux + 115, menuy + 293, 255, 255, 255, pFontSmall);
		}

		if (ESPYellowTeam)
		{
			FillRGB(menux + 21, menuy + 316, 89, 24, 255, 224, 32, 255);
			DrawString("Yellow", menux + 25, menuy + 318, 255, 255, 255, pFontSmall);
		}
		if (!ESPYellowTeam)
		{
			DrawGUIBox(menux + 20, menuy + 315, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Yellow", menux + 25, menuy + 318, 255, 255, 255, pFontSmall);
		}
		if (ESPWhiteTeam)
		{
			FillRGB(menux + 111, menuy + 316, 89, 24, 255, 255, 255, 255);
			DrawString("White", menux + 115, menuy + 318, 0, 0, 0, pFontSmall);
		}
		if (!ESPWhiteTeam)
		{
			DrawGUIBox(menux + 110, menuy + 315, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("White", menux + 115, menuy + 318, 255, 255, 255, pFontSmall);
		}

		//Glow colors menu team
		if (GlowRedTeam)
		{
			FillRGB(menux + 211, menuy + 266, 89, 24, 255, 0, 0, 255);
			DrawString("Red", menux + 215, menuy + 268, 255, 255, 255, pFontSmall);
		}
		if (!GlowRedTeam)
		{
			DrawGUIBox(menux + 210, menuy + 265, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Red", menux + 215, menuy + 268, 255, 255, 255, pFontSmall);
		}

		if (GlowGreenTeam)
		{
			FillRGB(menux + 301, menuy + 266, 89, 24, 0, 255, 0, 255);
			DrawString("Green", menux + 305, menuy + 268, 255, 255, 255, pFontSmall);
		}
		if (!GlowGreenTeam)
		{
			DrawGUIBox(menux + 300, menuy + 265, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Green", menux + 305, menuy + 268, 255, 255, 255, pFontSmall);
		}

		if (GlowBlueTeam)
		{
			FillRGB(menux + 211, menuy + 291, 89, 24, 0, 0, 255, 255);
			DrawString("Blue", menux + 215, menuy + 293, 255, 255, 255, pFontSmall);
		}
		if (!GlowBlueTeam)
		{
			DrawGUIBox(menux + 210, menuy + 290, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Blue", menux + 215, menuy + 293, 255, 255, 255, pFontSmall);
		}

		if (GlowPurpleTeam)
		{
			FillRGB(menux + 301, menuy + 291, 89, 24, 128, 0, 128, 255);
			DrawString("Purple", menux + 305, menuy + 293, 255, 255, 255, pFontSmall);
		}
		if (!GlowPurpleTeam)
		{
			DrawGUIBox(menux + 300, menuy + 290, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Purple", menux + 305, menuy + 293, 255, 255, 255, pFontSmall);
		}

		if (GlowYellowTeam)
		{
			FillRGB(menux + 211, menuy + 316, 89, 24, 255, 224, 32, 255);
			DrawString("Yellow", menux + 215, menuy + 318, 255, 255, 255, pFontSmall);
		}
		if (!GlowYellowTeam)
		{
			DrawGUIBox(menux + 210, menuy + 315, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Yellow", menux + 215, menuy + 318, 255, 255, 255, pFontSmall);
		}
		if (GlowWhiteTeam)
		{
			FillRGB(menux + 301, menuy + 316, 89, 24, 255, 255, 255, 255);
			DrawString("White", menux + 305, menuy + 318, 0, 0, 0, pFontSmall);
		}
		if (!GlowWhiteTeam)
		{
			DrawGUIBox(menux + 300, menuy + 315, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("White", menux + 305, menuy + 318, 255, 255, 255, pFontSmall);
		}

		//Chams colors menu team
		if (ChamRedTeam)
		{
			FillRGB(menux + 401, menuy + 266, 89, 24, 255, 0, 0, 255);
			DrawString("Red", menux + 405, menuy + 268, 255, 255, 255, pFontSmall);
		}
		if (!ChamRedTeam)
		{
			DrawGUIBox(menux + 400, menuy + 265, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Red", menux + 405, menuy + 268, 255, 255, 255, pFontSmall);
		}

		if (ChamGreenTeam)
		{
			FillRGB(menux + 491, menuy + 266, 89, 24, 0, 255, 0, 255);
			DrawString("Green", menux + 495, menuy + 268, 255, 255, 255, pFontSmall);
		}
		if (!ChamGreenTeam)
		{
			DrawGUIBox(menux + 490, menuy + 265, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Green", menux + 495, menuy + 268, 255, 255, 255, pFontSmall);
		}

		if (ChamBlueTeam)
		{
			FillRGB(menux + 401, menuy + 291, 89, 24, 0, 0, 255, 255);
			DrawString("Blue", menux + 405, menuy + 293, 255, 255, 255, pFontSmall);
		}
		if (!ChamBlueTeam)
		{
			DrawGUIBox(menux + 400, menuy + 290, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Blue", menux + 405, menuy + 293, 255, 255, 255, pFontSmall);
		}

		if (ChamPurpleTeam)
		{
			FillRGB(menux + 491, menuy + 291, 89, 24, 128, 0, 128, 255);
			DrawString("Purple", menux + 495, menuy + 293, 255, 255, 255, pFontSmall);
		}
		if (!ChamPurpleTeam)
		{
			DrawGUIBox(menux + 490, menuy + 290, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Purple", menux + 495, menuy + 293, 255, 255, 255, pFontSmall);
		}

		if (ChamYellowTeam)
		{
			FillRGB(menux + 401, menuy + 316, 89, 24, 255, 224, 32, 255);
			DrawString("Yellow", menux + 405, menuy + 318, 255, 255, 255, pFontSmall);
		}
		if (!ChamYellowTeam)
		{
			DrawGUIBox(menux + 400, menuy + 315, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Yellow", menux + 405, menuy + 318, 255, 255, 255, pFontSmall);
		}
		if (ChamWhiteTeam)
		{
			FillRGB(menux + 491, menuy + 316, 89, 24, 255, 255, 255, 255);
			DrawString("White", menux + 495, menuy + 318, 0, 0, 0, pFontSmall);
		}
		if (!ChamWhiteTeam)
		{
			DrawGUIBox(menux + 490, menuy + 315, 90, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("White", menux + 495, menuy + 318, 255, 255, 255, pFontSmall);
		}
	}

	if (OptionsMenu)
	{
		POINT p;
		GetCursorPos(&p);
		// Toggle Features
		if ((GetAsyncKeyState(VK_LBUTTON) & 1))
		{
			//Random menu color
			if ((p.x > menux + 275 && p.x < menux + 375) && (p.y > menuy + 45 && p.y < menuy + 80))
			{
				MenuR = 255;
				MenuG = 0;
				MenuB = 0;
				MenuColorRed = true;
				MenuColorOrange = false;
				MenuColorYellow = false;
				MenuColorLime = false;
				MenuColorGreen = false;
				MenuColorCyan = false;
				MenuColorBlue = false;
				MenuColorPurple = false;
				MenuColorMagenta = false;
				MenuColorPink = false;
				MenuColorBrown = false;
				MenuColorNavy = false;
			}
			if ((p.x > menux + 375 && p.x < menux + 475) && (p.y > menuy + 45 && p.y < menuy + 80))
			{
				MenuR = 255;
				MenuG = 128;
				MenuB = 0;
				MenuColorOrange = true;
				MenuColorRed = false;
				MenuColorYellow = false;
				MenuColorLime = false;
				MenuColorGreen = false;
				MenuColorCyan = false;
				MenuColorBlue = false;
				MenuColorPurple = false;
				MenuColorMagenta = false;
				MenuColorPink = false;
				MenuColorBrown = false;
				MenuColorNavy = false;
			}
			if ((p.x > menux + 475 && p.x < menux + 575) && (p.y > menuy + 45 && p.y < menuy + 80))
			{
				MenuR = 255;
				MenuG = 255;
				MenuB = 0;
				MenuColorYellow = true;
				MenuColorRed = false;
				MenuColorOrange = false;
				MenuColorLime = false;
				MenuColorGreen = false;
				MenuColorCyan = false;
				MenuColorBlue = false;
				MenuColorPurple = false;
				MenuColorMagenta = false;
				MenuColorPink = false;
				MenuColorBrown = false;
				MenuColorNavy = false;
			}
			if ((p.x > menux + 275 && p.x < menux + 375) && (p.y > menuy + 70 && p.y < menuy + 95))
			{
				MenuR = 128;
				MenuG = 255;
				MenuB = 0;
				MenuColorLime = true;
				MenuColorRed = false;
				MenuColorOrange = false;
				MenuColorYellow = false;
				MenuColorGreen = false;
				MenuColorCyan = false;
				MenuColorBlue = false;
				MenuColorPurple = false;
				MenuColorMagenta = false;
				MenuColorPink = false;
				MenuColorBrown = false;
				MenuColorNavy = false;
			}
			if ((p.x > menux + 375 && p.x < menux + 475) && (p.y > menuy + 70 && p.y < menuy + 95))
			{
				MenuR = 0;
				MenuG = 255;
				MenuB = 0;
				MenuColorGreen = true;
				MenuColorRed = false;
				MenuColorOrange = false;
				MenuColorYellow = false;
				MenuColorLime = false;
				MenuColorCyan = false;
				MenuColorBlue = false;
				MenuColorPurple = false;
				MenuColorMagenta = false;
				MenuColorPink = false;
				MenuColorBrown = false;
				MenuColorNavy = false;
			}
			if ((p.x > menux + 475 && p.x < menux + 575) && (p.y > menuy + 70 && p.y < menuy + 95))
			{
				MenuR = 51;
				MenuG = 255;
				MenuB = 255;
				MenuColorCyan = true;
				MenuColorRed = false;
				MenuColorOrange = false;
				MenuColorYellow = false;
				MenuColorLime = false;
				MenuColorGreen = false;
				MenuColorBlue = false;
				MenuColorPurple = false;
				MenuColorMagenta = false;
				MenuColorPink = false;
				MenuColorBrown = false;
				MenuColorNavy = false;
			}
			if ((p.x > menux + 275 && p.x < menux + 375) && (p.y > menuy + 95 && p.y < menuy + 120))
			{
				MenuR = 0;
				MenuG = 0;
				MenuB = 255;
				MenuColorBlue = true;
				MenuColorRed = false;
				MenuColorOrange = false;
				MenuColorYellow = false;
				MenuColorLime = false;
				MenuColorGreen = false;
				MenuColorCyan = false;
				MenuColorPurple = false;
				MenuColorMagenta = false;
				MenuColorPink = false;
				MenuColorBrown = false;
				MenuColorNavy = false;
			}
			if ((p.x > menux + 375 && p.x < menux + 475) && (p.y > menuy + 95 && p.y < menuy + 120))
			{
				MenuR = 102;
				MenuG = 0;
				MenuB = 51;
				MenuColorPurple = true;
				MenuColorRed = false;
				MenuColorOrange = false;
				MenuColorYellow = false;
				MenuColorLime = false;
				MenuColorGreen = false;
				MenuColorCyan = false;
				MenuColorBlue = false;
				MenuColorMagenta = false;
				MenuColorPink = false;
				MenuColorBrown = false;
				MenuColorNavy = false;
			}
			if ((p.x > menux + 475 && p.x < menux + 575) && (p.y > menuy + 95 && p.y < menuy + 120))
			{
				MenuR = 255;
				MenuG = 0;
				MenuB = 127;
				MenuColorMagenta = true;
				MenuColorRed = false;
				MenuColorOrange = false;
				MenuColorYellow = false;
				MenuColorLime = false;
				MenuColorGreen = false;
				MenuColorCyan = false;
				MenuColorBlue = false;
				MenuColorPurple = false;
				MenuColorPink = false;
				MenuColorBrown = false;
				MenuColorNavy = false;
			}
			if ((p.x > menux + 275 && p.x < menux + 375) && (p.y > menuy + 120 && p.y < menuy + 145))
			{
				MenuR = 255;
				MenuG = 102;
				MenuB = 178;
				MenuColorPink = true;
				MenuColorRed = false;
				MenuColorOrange = false;
				MenuColorYellow = false;
				MenuColorLime = false;
				MenuColorGreen = false;
				MenuColorCyan = false;
				MenuColorBlue = false;
				MenuColorPurple = false;
				MenuColorMagenta = false;
				MenuColorBrown = false;
				MenuColorNavy = false;
			}
			if ((p.x > menux + 375 && p.x < menux + 475) && (p.y > menuy + 120 && p.y < menuy + 145))
			{
				MenuR = 102;
				MenuG = 51;
				MenuB = 0;
				MenuColorBrown = true;
				MenuColorRed = false;
				MenuColorOrange = false;
				MenuColorYellow = false;
				MenuColorLime = false;
				MenuColorGreen = false;
				MenuColorCyan = false;
				MenuColorBlue = false;
				MenuColorPurple = false;
				MenuColorMagenta = false;
				MenuColorPink = false;
				MenuColorNavy = false;
			}
			if ((p.x > menux + 475 && p.x < menux + 575) && (p.y > menuy + 120 && p.y < menuy + 145))
			{
				MenuR = 0;
				MenuG = 51;
				MenuB = 102;
				MenuColorNavy = true;
				MenuColorRed = false;
				MenuColorOrange = false;
				MenuColorYellow = false;
				MenuColorLime = false;
				MenuColorGreen = false;
				MenuColorCyan = false;
				MenuColorBlue = false;
				MenuColorPurple = false;
				MenuColorMagenta = false;
				MenuColorPink = false;
				MenuColorBrown = false;
			}
			if ((p.x > menux + 275 && p.x < menux + 575) && (p.y > menuy + 145 && p.y < menuy + 170))
			{
				RainbowFeatures();
			}
		}
		FillRGB(menux + 1, menuy - 40, 199, 40, MenuR, MenuG, MenuB, 150);
		FillRGB(menux + 200, menuy - 40, 200, 40, MenuR, MenuG, MenuB, 150);
		DrawGUIBox(menux + 400, menuy - 40, 199, 39, 255, 255, 255, 230, MenuR, MenuG, MenuB, 230);

		DrawGUIBox(menux + 20, menuy + 20, 230, 125, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		FillRGB(menux + 20, menuy + 20, 230, 25, MenuR, MenuG, MenuB, 230);
		DrawString("Keymap:", menux + 26, menuy + 23, 255, 255, 255, pFontSmall);

		DrawGUIBox(menux + 20, menuy + 45, 230, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		DrawString("Aimbot = [X]", menux + 25, menuy + 48, 255, 255, 255, pFontSmall);
		DrawGUIBox(menux + 20, menuy + 70, 230, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		DrawString("Bunnyhop = [SPACE]", menux + 25, menuy + 73, 255, 255, 255, pFontSmall);
		DrawGUIBox(menux + 20, menuy + 95, 230, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		DrawString("View Menu = [INSERT]", menux + 25, menuy + 98, 255, 255, 255, pFontSmall);
		DrawGUIBox(menux + 20, menuy + 120, 230, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		DrawString("Terminate Hacks = [F9]", menux + 25, menuy + 123, 255, 255, 255, pFontSmall);

		DrawGUIBox(menux + 275, menuy + 20, 300, 125, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		FillRGB(menux + 275, menuy + 20, 300, 25, MenuR, MenuG, MenuB, 230);
		DrawString("Menu Color:", menux + 281, menuy + 23, 255, 255, 255, pFontSmall);

		if (MenuColorRed)
		{
			FillRGB(menux + 276, menuy + 46, 99, 24, 255, 0, 0, 230);
			DrawString("Red", menux + 280, menuy + 48, 255, 255, 255, pFontSmall);
		}
		if (!MenuColorRed)
		{
			DrawGUIBox(menux + 275, menuy + 45, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Red", menux + 280, menuy + 48, 255, 255, 255, pFontSmall);
		}

		if (MenuColorOrange)
		{
			FillRGB(menux + 376, menuy + 46, 99, 24, 255, 128, 0, 230);
			DrawString("Orange", menux + 380, menuy + 48, 255, 255, 255, pFontSmall);
		}
		if (!MenuColorOrange)
		{
			DrawGUIBox(menux + 375, menuy + 45, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Orange", menux + 380, menuy + 48, 255, 255, 255, pFontSmall);
		}
		
		if (MenuColorYellow)
		{
			FillRGB(menux + 476, menuy + 46, 99, 24, 255, 255, 0, 230);
			DrawString("Yellow", menux + 480, menuy + 48, 255, 255, 255, pFontSmall);
		}
		if (!MenuColorYellow)
		{
			DrawGUIBox(menux + 475, menuy + 45, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Yellow", menux + 480, menuy + 48, 255, 255, 255, pFontSmall);
		}
		
		if (MenuColorLime)
		{
			FillRGB(menux + 276, menuy + 71, 99, 24, 128, 255, 0, 230);
			DrawString("Lime", menux + 280, menuy + 73, 255, 255, 255, pFontSmall);
		}
		if (!MenuColorLime)
		{
			DrawGUIBox(menux + 275, menuy + 70, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Lime", menux + 280, menuy + 73, 255, 255, 255, pFontSmall);
		}
		
		if (MenuColorGreen)
		{
			FillRGB(menux + 376, menuy + 71, 99, 24, 0, 153, 0, 230);
			DrawString("Green", menux + 380, menuy + 73, 255, 255, 255, pFontSmall);
		}
		if (!MenuColorGreen)
		{
			DrawGUIBox(menux + 375, menuy + 70, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Green", menux + 380, menuy + 73, 255, 255, 255, pFontSmall);
		}
		
		if (MenuColorCyan)
		{
			FillRGB(menux + 476, menuy + 71, 99, 24, 51, 255, 255, 230);
			DrawString("Cyan", menux + 480, menuy + 73, 255, 255, 255, pFontSmall);
		}
		if (!MenuColorCyan)
		{
			DrawGUIBox(menux + 475, menuy + 70, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Cyan", menux + 480, menuy + 73, 255, 255, 255, pFontSmall);
		}

		if (MenuColorBlue)
		{
			FillRGB(menux + 276, menuy + 96, 99, 24, 0, 0, 255, 230);
			DrawString("Blue", menux + 280, menuy + 98, 255, 255, 255, pFontSmall);
		}
		if (!MenuColorBlue)
		{
			DrawGUIBox(menux + 275, menuy + 95, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Blue", menux + 280, menuy + 98, 255, 255, 255, pFontSmall);
		}
		
		if (MenuColorPurple)
		{
			FillRGB(menux + 376, menuy + 96, 99, 24, 153, 0, 76, 230);
			DrawString("Purple", menux + 380, menuy + 98, 255, 255, 255, pFontSmall);
		}
		if (!MenuColorPurple)
		{
			DrawGUIBox(menux + 375, menuy + 95, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Purple", menux + 380, menuy + 98, 255, 255, 255, pFontSmall);
		}
		
		if (MenuColorMagenta)
		{
			FillRGB(menux + 476, menuy + 96, 99, 24, 255, 0, 127, 230);
			DrawString("Magenta", menux + 480, menuy + 98, 255, 255, 255, pFontSmall);
		}
		if (!MenuColorMagenta)
		{
			DrawGUIBox(menux + 475, menuy + 95, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Magenta", menux + 480, menuy + 98, 255, 255, 255, pFontSmall);
		}
		
		if (MenuColorPink)
		{
			FillRGB(menux + 276, menuy + 121, 99, 24, 255, 102, 178, 230);
			DrawString("Pink", menux + 280, menuy + 123, 255, 255, 255, pFontSmall);
		}
		if (!MenuColorPink)
		{
			DrawGUIBox(menux + 275, menuy + 120, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Pink", menux + 280, menuy + 123, 255, 255, 255, pFontSmall);
		}
		
		if (MenuColorBrown)
		{
			FillRGB(menux + 376, menuy + 121, 99, 24, 102, 51, 0, 230);
			DrawString("Brown", menux + 380, menuy + 123, 255, 255, 255, pFontSmall);
		}
		if (!MenuColorBrown)
		{
			DrawGUIBox(menux + 375, menuy + 120, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Brown", menux + 380, menuy + 123, 255, 255, 255, pFontSmall);
		}

		if (MenuColorNavy)
		{
			FillRGB(menux + 476, menuy + 121, 99, 24, 0, 51, 102, 230);
			DrawString("Navy", menux + 480, menuy + 123, 255, 255, 255, pFontSmall);
		}
		if (!MenuColorNavy)
		{
			DrawGUIBox(menux + 475, menuy + 120, 100, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
			DrawString("Navy", menux + 480, menuy + 123, 255, 255, 255, pFontSmall);
		}
		
		DrawGUIBox(menux + 275, menuy + 145, 300, 25, MenuR, MenuG, MenuB, 230, AccentR, AccentG, AccentB, 230);
		FillRGB(menux + 275, menuy + 145, 300, 25, MenuR, MenuG, MenuB, 230);
		DrawString("Random Color", menux + 366, menuy + 148, 255, 255, 255, pFontSmall);
	}
	Sleep(1);
}

void RainbowFeatures()
{
	/*if (RainbowMenu)
	{*/
		int RandomColorR = rand() % 255 + 1;
		int RandomColorG = rand() % 255 + 1;
		int RandomColorB = rand() % 255 + 1;
		MenuR = RandomColorR;
		MenuG = RandomColorG;
		MenuB = RandomColorB;
	/*}*/
	/*if (RainbowCrosshair)
	{
			int RandomColorR1 = rand() % 255 + 1;
			int RandomColorG1 = rand() % 255 + 1;
			int RandomColorB1 = rand() % 255 + 1;
			IdleCrossR = RandomColorR1;
			IdleCrossG = RandomColorG1;
			IdleCrossB = RandomColorB1;

			int RandomColorR = rand() % 255 + 1;
			int RandomColorG = rand() % 255 + 1;
			int RandomColorB = rand() % 255 + 1;
			SpottedCrossR = RandomColorR;
			SpottedCrossG = RandomColorG;
			SpottedCrossB = RandomColorB;
	}*/
}