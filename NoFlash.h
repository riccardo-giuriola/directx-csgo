#pragma once

#include "Includes.h"

DWORD PlayerBase;
float FlashValue;

struct PlayerDataNOFlash
{
	float Flash;

	void readMemory()
	{
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwLocalPlayer), &PlayerBase, sizeof(DWORD), NULL);
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(PlayerBase + hazedumper::netvars::m_flFlashMaxAlpha), &Flash, sizeof(float), NULL);
	}
}PlayerDataNOFlash;

void runNoFlash()
{
	while (true)
	{
		PlayerDataNOFlash.readMemory();
		if (PlayerDataNOFlash.Flash > 0.0f)
		{
			FlashValue = 0.0f;
			WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(PlayerBase + hazedumper::netvars::m_flFlashMaxAlpha), &FlashValue, sizeof(float), NULL);
		}
		/*else if (PlayerDataNOFlash.Flash == 0.0f)
		{
			FlashValue = 255.0f;
			WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(PlayerBase + hazedumper::netvars::m_flFlashMaxAlpha), &FlashValue, sizeof(float), NULL);
		}*/
		Sleep(1);
	}
}