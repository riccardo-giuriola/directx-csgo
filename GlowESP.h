#pragma once

#include "Includes.h"

using namespace std;

DWORD GlowBase;
DWORD GlowIndex;

typedef struct 
{
	float r = 255;
	float g = 0;
	float b = 0;
	float a = 255;
} rgba;

rgba color;

typedef struct
{
	bool rwo = true;
	/*bool rwuo = false;*/
} rwo;

rwo rwuo;

struct PlayerDataGlow
{
	DWORD PlayerBase;
	int Team;
	void Read()
	{
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwLocalPlayer), &PlayerBase, sizeof(DWORD), NULL);
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(PlayerBase + hazedumper::netvars::m_iTeamNum), &Team, sizeof(int), NULL);
	}
}PlayerDataGlow;

struct EntityList
{
	DWORD EntityBase;
	int Team;
	void Read(int i)
	{
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwGlowObjectManager), &GlowBase, sizeof(DWORD), NULL);
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(fProcess.__dwordClient + hazedumper::signatures::dwEntityList + (i * 0x10)), &EntityBase, sizeof(DWORD), NULL);
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(EntityBase + hazedumper::netvars::m_iGlowIndex), &GlowIndex, sizeof(DWORD), NULL);
		ReadProcessMemory(fProcess.__HandleProcess, (PBYTE*)(EntityBase + hazedumper::netvars::m_iTeamNum), &Team, sizeof(int), NULL);
	}
}EntityList[NumOfPlayers];

void WriteMemory()
{
	WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(GlowBase + (GlowIndex * 0x38) + 0x4), &color, sizeof(rgba), NULL);
	/*WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(GlowBase + (GlowIndex * 0x38) + 0x8), &g, sizeof(float), NULL);
	WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(GlowBase + (GlowIndex * 0x38) + 0xC), &b, sizeof(float), NULL);
	WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(GlowBase + (GlowIndex * 0x38) + 0x10), &a, sizeof(float), NULL);*/

	WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(GlowBase + (GlowIndex * 0x38) + 0x24), &rwuo, sizeof(bool), NULL);
	/*WriteProcessMemory(fProcess.__HandleProcess, (PBYTE*)(GlowBase + (GlowIndex * 0x38) + 0x25), &rwuo, sizeof(bool), NULL);*/
}

void Glow()
{
	PlayerDataGlow.Read();
	for (int i = 0; i < NumOfPlayers; i++)
	{
		EntityList[i].Read(i);

		if (EntityList[i].Team == PlayerDataGlow.Team)
		{
			color.r = GlowTeamColorR;
			color.g = GlowTeamColorG;
			color.b = GlowTeamColorB;
		}
		if(EntityList[i].Team == PlayerDataGlow.Team - 1 || EntityList[i].Team == PlayerDataGlow.Team + 1)
		{
			color.r = GlowEnemyColorR;
			color.g = GlowEnemyColorG;
			color.b = GlowEnemyColorB;
		}
		WriteMemory();
	}
}

void runGlowESP()
{
	while (true)
	{
		Glow();
	}
}